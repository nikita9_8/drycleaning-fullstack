import React from 'react';

import { AuthNavigation } from './Navigations/AuthNavigator';
import { useAuth } from './hooks/useAuth';
import { AdminNavigator } from './Navigations/AdminNavigator';
import { ClientNavigator } from './Navigations/ClientNavigator';

export const Main = () => {
    const {
        user: { role },
        token: { token },
    } = useAuth();

    return (
        <>
            {token ? (
                <>
                    {role === 'admin' ? (
                        <AdminNavigator />
                    ) : (
                        <ClientNavigator />
                    )}
                </>
            ) : (
                <>
                    <AuthNavigation />
                </>
            )}
        </>
    );
};
