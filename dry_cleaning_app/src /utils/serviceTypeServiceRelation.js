export const ServiceTypeServiceRelation = (data) =>
    data.map((item) => {
        const serviceTypes = {};
        const services = {};

        const groupes = item.dryCleaningServices.reduce((res, reg) => {
            if (!res.serviceTypes) res.serviceTypes = [];
            if (!serviceTypes[reg.serviceType.name]) {
                const serviceType = {
                    ...reg.serviceType,
                    services: [],
                };
                res.serviceTypes.push(serviceType);
                serviceTypes[reg.serviceType.name] = serviceType;
            }
            if (!services[reg.service.name]) {
                const service = {
                    ...reg.service,
                    cost: reg.pivot ? reg.pivot.cost : reg.cost,
                };
                serviceTypes[reg.serviceType.name].services.push(service);
                services[reg.service.name] = service;
            }
            return res;
        }, {});

        return {
            ...item,
            dryCleaningServices: Object.keys(groupes).length
                ? groupes.serviceTypes.map((newItem) => ({
                      ...newItem,
                      title: newItem.name,
                      data: newItem.services,
                  }))
                : [],
        };
    });
