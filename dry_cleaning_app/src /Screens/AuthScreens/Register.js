import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import {
    Container,
    Form,
    Item,
    Input,
    Label,
    Button,
    Text,
    Picker,
} from 'native-base';
import { Formik } from 'formik';
import { useDispatch } from 'react-redux';

import { validationRegister } from '../../validators/validationRegister';
import { userRegisterAction } from '../../store/actions/userAction';

const styles = StyleSheet.create({
    btn: {
        width: '70%',
        alignSelf: 'center',
    },
    wrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
        flex: 1,
    },
    fullWidth: {
        width: '100%',
    },
    validateText: {
        color: 'red',
    },
    btnGroup: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginTop: 40,
    },
    btnItem: {
        width: '40%',
        justifyContent: 'center',
    },
    btnText: {
        fontSize: 10,
    },
});

export const Register = ({ navigation }) => {
    const dispatch = useDispatch();
    const [loginData, setLoginData] = useState({
        email: '',
        password: '',
        username: '',
        userRole: 'client',
    });

    const signUpHandler = (value) => {
        dispatch(userRegisterAction(value));
    };

    const changeAuthPage = (page) => {
        navigation.replace(page);
    };

    return (
        <Container style={styles.container}>
            <View style={styles.wrapper}>
                <Formik
                    initialValues={loginData}
                    onSubmit={signUpHandler}
                    validate={validationRegister}
                >
                    {({
                        handleChange,
                        handleSubmit,
                        values,
                        errors,
                        isSubmitting,
                    }) => (
                        <View style={styles.fullWidth}>
                            <Form>
                                <Item stackedLabel>
                                    <Label>Email</Label>
                                    <Input
                                        onChangeText={handleChange('email')}
                                        value={values.email}
                                    />
                                </Item>
                                <Text style={styles.validateText}>
                                    {errors.email}
                                </Text>
                                <Item stackedLabel>
                                    <Label>Password</Label>
                                    <Input
                                        onChangeText={handleChange('password')}
                                        value={values.password}
                                        error={errors.password}
                                    />
                                </Item>
                                <Text style={styles.validateText}>
                                    {errors.password}
                                </Text>
                                <Item stackedLabel>
                                    <Label>Username</Label>
                                    <Input
                                        onChangeText={handleChange('username')}
                                        value={values.username}
                                        error={errors.username}
                                    />
                                </Item>
                                <Text style={styles.validateText}>
                                    {errors.username}
                                </Text>

                                <Picker
                                    selectedValue={values.userRole}
                                    onValueChange={handleChange('userRole')}
                                >
                                    <Picker.Item label="Admin" value="admin" />
                                    <Picker.Item
                                        label="Client"
                                        value="client"
                                    />
                                </Picker>

                                <Button
                                    primary
                                    block
                                    style={styles.btn}
                                    onPress={handleSubmit}
                                    disabled={!!Object.values(errors).length}
                                >
                                    <Text>Sign up</Text>
                                </Button>
                            </Form>
                            <View style={styles.btnGroup}>
                                <Button
                                    bordered
                                    style={styles.btnItem}
                                    onPress={() => changeAuthPage('Login')}
                                >
                                    <Text style={styles.btnText}>Sign in</Text>
                                </Button>
                                <Button
                                    bordered
                                    style={styles.btnItem}
                                    onPress={() => changeAuthPage('Restore')}
                                >
                                    <Text style={styles.btnText}>
                                        Restore password
                                    </Text>
                                </Button>
                            </View>
                        </View>
                    )}
                </Formik>
            </View>
        </Container>
    );
};
