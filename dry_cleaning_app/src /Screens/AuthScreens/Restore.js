import React, { useState } from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';
import { Container, Form, Item, Input, Label, Button, Text } from 'native-base';
import { Formik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';

import { validationLogin } from '../../validators/validationLogin';
import { userRestoreAction } from '../../store/actions/userAction';

const styles = StyleSheet.create({
    btn: {
        width: '70%',
        alignSelf: 'center',
    },
    wrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
        flex: 1,
    },
    fullWidth: {
        width: '100%',
    },
    validateText: {
        color: 'red',
    },
    btnGroup: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginTop: 40,
    },
    btnItem: {
        width: '40%',
        justifyContent: 'center',
    },
    btnText: {
        fontSize: 10,
    },
});

export const Restore = ({ navigation }) => {
    const dispatch = useDispatch();
    const [loginData, setLoginData] = useState({
        email: '',
        password: '',
    });

    const changeAuthPage = (page) => {
        navigation.replace(page);
    };

    const restoreHandler = (value) => {
        dispatch(userRestoreAction(value));
        changeAuthPage('Login');
    };

    return (
        <Container style={styles.container}>
            <View style={styles.wrapper}>
                <Formik
                    initialValues={loginData}
                    onSubmit={restoreHandler}
                    validate={validationLogin}
                >
                    {({
                        handleChange,
                        handleSubmit,
                        values,
                        errors,
                        isSubmitting,
                    }) => (
                        <View style={styles.fullWidth}>
                            <Form>
                                <Item stackedLabel>
                                    <Label>Email</Label>
                                    <Input
                                        onChangeText={handleChange('email')}
                                        value={values.email}
                                    />
                                </Item>
                                <Text style={styles.validateText}>
                                    {errors.email}
                                </Text>
                                <Item stackedLabel>
                                    <Label>Password</Label>
                                    <Input
                                        onChangeText={handleChange('password')}
                                        value={values.password}
                                        error={errors.password}
                                    />
                                </Item>
                                <Text style={styles.validateText}>
                                    {errors.password}
                                </Text>

                                <Button
                                    primary
                                    block
                                    style={styles.btn}
                                    onPress={handleSubmit}
                                    disabled={!!Object.values(errors).length}
                                >
                                    <Text>Restore</Text>
                                </Button>
                            </Form>
                            <View style={styles.btnGroup}>
                                <Button
                                    bordered
                                    style={styles.btnItem}
                                    onPress={() => changeAuthPage('Login')}
                                >
                                    <Text style={styles.btnText}>Sign in</Text>
                                </Button>
                                <Button
                                    bordered
                                    style={styles.btnItem}
                                    onPress={() => changeAuthPage('Register')}
                                >
                                    <Text style={styles.btnText}>Sign up</Text>
                                </Button>
                            </View>
                        </View>
                    )}
                </Formik>
            </View>
        </Container>
    );
};
