import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { useAuth } from '../../../hooks/useAuth';
import { orderPutAction } from '../../../store/actions/orderAction';
import { DraftOrder } from '../../../Components/OrderComponent/DraftOrder';

export const EditOrder = ({ route, navigation }) => {
    const { id } = route.params;
    const { token } = useAuth();
    const dispatch = useDispatch();
    const order = useSelector((state) =>
        state.order.orders.find((item) => item.id === id)
    );

    const submitEditHandler = (data) => {
        data.id = id;
        dispatch(orderPutAction(token, data));
        navigation.goBack();
    };

    useEffect(() => {
        navigation.setOptions({
            title: `Edit: Order ${id}`,
        });
    }, [order, id, navigation]);

    return <DraftOrder data={order} submitEditHandler={submitEditHandler} />;
};
