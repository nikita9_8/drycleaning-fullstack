import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { allOrdersAction } from '../../../store/actions/orderAction';

import { AppActivityIndicator } from '../../../Components/AppActivityIndicator';
import { useAuth } from '../../../hooks/useAuth';
import { AbstractList } from '../../../Components/AbstractList';
import { ListItem } from '../../../Components/OrderComponent/ListItem';

export const OrderList = ({ navigation }) => {
    const { token, user } = useAuth();
    const dispatch = useDispatch();
    const orderList = useSelector((state) => state.order.orders);
    const loading = useSelector((state) => state.order.loading);

    useEffect(() => {
        dispatch(allOrdersAction(token, user.role));
    }, [dispatch, orderList.length]);

    const refreshHandler = () => {
        dispatch(allOrdersAction(token, user.role));
    };

    const viewDetailHandler = (id) => {
        navigation.navigate('Order', {
            id,
            name: `Order # ${id}`,
        });
    };

    return (
        <>
            {loading ? (
                <AppActivityIndicator />
            ) : (
                <AbstractList
                    data={orderList}
                    viewDetailHandler={viewDetailHandler}
                    render={(item, handler) => (
                        <ListItem itemData={item} viewDetailHandler={handler} />
                    )}
                    refreshHandler={refreshHandler}
                    refreshing={loading}
                />
            )}
        </>
    );
};
