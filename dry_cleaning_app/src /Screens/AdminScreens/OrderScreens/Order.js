import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { ListItemDetail } from '../../../Components/OrderComponent/ListItemDetail';
import { AppActivityIndicator } from '../../../Components/AppActivityIndicator';
import { dryCleaningDeleteAction } from '../../../store/actions/dryCleaningsAction';
import { useAuth } from '../../../hooks/useAuth';
import { deleteOrderAction } from '../../../store/actions/orderAction';

export const Order = ({ route, navigation }) => {
    const { id } = route.params;
    const dispatch = useDispatch();
    const { token, user } = useAuth();
    const isLoading = useSelector((state) => state.order.loading);
    const order = useSelector((state) =>
        state.order.orders.find((item) => item.id === id)
    );

    const editHandler = () => {
        navigation.navigate('EditOrder', {
            id,
            name: `Edit: Order ${order.id}`,
        });
    };

    const removeHandler = () => {
        dispatch(deleteOrderAction(token, id));
        navigation.goBack();
    };

    useEffect(() => {
        navigation.setOptions({
            title: order?.id ? `Order # ${order?.id}` : '',
        });
    }, [order, id, navigation]);

    if (isLoading) {
        return <AppActivityIndicator />;
    }

    return (
        <ListItemDetail
            data={order}
            editHandler={editHandler}
            removeHandler={removeHandler}
            user={user}
        />
    );
};
