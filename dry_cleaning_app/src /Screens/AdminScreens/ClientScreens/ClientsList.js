import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { allClients } from '../../../store/actions/userAction';

import { AppActivityIndicator } from '../../../Components/AppActivityIndicator';
import { useAuth } from '../../../hooks/useAuth';
import { AbstractList } from '../../../Components/AbstractList';
import { ListItem } from '../../../Components/ClientComponent/ListItem';
import { allOrdersAction } from '../../../store/actions/orderAction';

export const ClientList = ({ navigation }) => {
    const { token } = useAuth();
    const dispatch = useDispatch();
    const userList = useSelector((state) => state.user.allUsers);
    const loading = useSelector((state) => state.user.loading);

    useEffect(() => {
        dispatch(allClients(token));
    }, [dispatch]);

    const refreshHandler = () => {
        dispatch(allClients(token));
    };

    const viewDetailHandler = (id, name) => {
        navigation.navigate('Client', {
            id,
            name,
        });
    };

    return (
        <>
            {loading ? (
                <AppActivityIndicator />
            ) : (
                <AbstractList
                    data={userList}
                    viewDetailHandler={viewDetailHandler}
                    render={(item, handler) => (
                        <ListItem itemData={item} viewDetailHandler={handler} />
                    )}
                    refreshHandler={refreshHandler}
                    refreshing={loading}
                />
            )}
        </>
    );
};
