import React from 'react';
import { useSelector } from 'react-redux';
import { Text, StyleSheet, View } from 'react-native';

const styles = StyleSheet.create({
    wrapper: {
        margin: 20,
    },
    info: {
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 10,
    },
    text: {
        fontSize: 18,
        marginVertical: 5,
    },
});

export const Client = ({ route }) => {
    const { id } = route.params;
    const client = useSelector((state) =>
        state.user.allUsers.find((item) => item.id === id)
    );

    return (
        <View style={styles.wrapper}>
            <Text style={styles.info}>Client Info</Text>
            <Text style={styles.text}>username: {client.username}</Text>
            <Text style={styles.text}>email: {client.email}</Text>
            <Text style={styles.text}>role: {client.role}</Text>
            <Text style={styles.text}>money: {client.money} $</Text>
        </View>
    );
};
