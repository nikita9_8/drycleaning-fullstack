import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { ListItemDetail } from '../../../Components/DryCleaningComponents/ListItemDetail';
import { AppActivityIndicator } from '../../../Components/AppActivityIndicator';
import { dryCleaningDeleteAction } from '../../../store/actions/dryCleaningsAction';
import { createOrderAction } from '../../../store/actions/orderAction';
import { useAuth } from '../../../hooks/useAuth';

export const DryCleaning = ({ route, navigation }) => {
    const { id } = route.params;
    const dispatch = useDispatch();
    const { token } = useAuth();
    const isLoading = useSelector((state) => state.dryCleaning.loading);
    const dryCleaning = useSelector((state) =>
        state.dryCleaning.dryCleaningList.find((item) => item.id === id)
    );

    const editHandler = () => {
        navigation.navigate('EditDryCleaning', {
            id,
            name: `Edit: ${dryCleaning.name}`,
        });
    };

    const removeHandler = () => {
        dispatch(dryCleaningDeleteAction(id, token));
        navigation.goBack();
    };

    const createOrderHandler = (services) => {
        const serviceTypeGroups = [];
        [...dryCleaning.dryCleaningServices].forEach((item) => {
            const serviceTypeServices = item.data.filter((service) => {
                const findedService = services.find(
                    (selectedService) => selectedService.name === service.name
                );

                if (findedService) {
                    return findedService;
                }
                return false;
            });

            if (serviceTypeServices.length) {
                serviceTypeGroups.push({
                    serviceType: item.title,
                    services: serviceTypeServices,
                });
            }
        });
        dispatch(createOrderAction(token, serviceTypeGroups, dryCleaning.id));
        navigation.goBack();
    };

    useEffect(() => {
        navigation.setOptions({
            title: dryCleaning?.name ?? '',
        });
    }, [dryCleaning, id, navigation]);

    if (isLoading) {
        return <AppActivityIndicator />;
    }

    return (
        <ListItemDetail
            data={dryCleaning}
            editHandler={editHandler}
            removeHandler={removeHandler}
            createOrder={createOrderHandler}
        />
    );
};
