import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { DraftDryCleaning } from '../../../Components/DryCleaningComponents/DraftDryCleaning';
import { useAuth } from '../../../hooks/useAuth';
import { dryCleaningPutAction } from '../../../store/actions/dryCleaningsAction';

export const EditDryCleaning = ({ route, navigation }) => {
    const { id } = route.params;
    const { token } = useAuth();
    const dispatch = useDispatch();
    const dryCleaning = useSelector((state) =>
        state.dryCleaning.dryCleaningList.find((item) => item.id === id)
    );

    const submitEditHandler = (data) => {
        data.id = id;
        dispatch(dryCleaningPutAction(data, token));
        navigation.goBack();
    };

    useEffect(() => {
        navigation.setOptions({
            title: `Edit: ${dryCleaning.name}`,
        });
    }, [dryCleaning, id, navigation]);

    return (
        <DraftDryCleaning
            data={dryCleaning}
            submitEditHandler={submitEditHandler}
        />
    );
};
