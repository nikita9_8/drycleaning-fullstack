import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { DraftDryCleaning } from '../../../Components/DryCleaningComponents/DraftDryCleaning';
import { useAuth } from '../../../hooks/useAuth';
import { dryCleaningCreateAction } from '../../../store/actions/dryCleaningsAction';

export const CreateDryCleaning = ({ navigation }) => {
    const { token } = useAuth();
    const dispatch = useDispatch();

    const submitEditHandler = (data) => {
        dispatch(dryCleaningCreateAction(data, token));
        navigation.navigate('DryCleaning');
    };

    useEffect(() => {
        navigation.setOptions({
            title: `New dry cleaning`,
        });
    }, []);

    return <DraftDryCleaning submitEditHandler={submitEditHandler} />;
};
