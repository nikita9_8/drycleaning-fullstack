import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { dryCleaningListAction } from '../../../store/actions/dryCleaningsAction';

import { AppActivityIndicator } from '../../../Components/AppActivityIndicator';
import { List } from '../../../Components/DryCleaningComponents/List';
import { useAuth } from '../../../hooks/useAuth';
import { AbstractList } from '../../../Components/AbstractList';
import { ListItem } from '../../../Components/DryCleaningComponents/ListItem';
import { allClients } from '../../../store/actions/userAction';

export const DryCleaningList = ({ navigation }) => {
    const { token } = useAuth();
    const dispatch = useDispatch();
    const dryCleaningList = useSelector(
        (state) => state.dryCleaning.dryCleaningList
    );
    const loading = useSelector((state) => state.dryCleaning.loading);

    useEffect(() => {
        dispatch(dryCleaningListAction(token));
    }, [dispatch]);

    const refreshHandler = () => {
        dispatch(dryCleaningListAction(token));
    };

    const viewDetailHandler = (id, name) => {
        navigation.navigate('DryCleaning', {
            id,
            name,
        });
    };

    return (
        <>
            {loading ? (
                <AppActivityIndicator />
            ) : (
                <AbstractList
                    data={dryCleaningList}
                    viewDetailHandler={viewDetailHandler}
                    render={(item, handler) => (
                        <ListItem itemData={item} viewDetailHandler={handler} />
                    )}
                    refreshHandler={refreshHandler}
                    refreshing={loading}
                />
            )}
        </>
    );
};
