export const validationRegister = (values) => {
    const errors = {};
    if (!values.password) {
        errors.password = 'Required';
    } else if (values.password.length < 5) {
        errors.password = 'Must be more then 5';
    } else if (values.password.length > 20) {
        errors.password = 'Must be more 20 or less';
    }

    if (!values.username) {
        errors.username = 'Required';
    } else if (values.username.length < 5) {
        errors.username = 'Must be more then 5';
    } else if (values.username.length > 20) {
        errors.username = 'Must be more 20 or less';
    }

    if (!values.email) {
        errors.email = 'Required';
    } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
        errors.email = 'Invalid email address';
    }

    return errors;
};
