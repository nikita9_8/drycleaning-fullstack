import React from 'react';
import { StyleSheet, FlatList, View } from 'react-native';

import { NoData } from './NoData';

const styles = StyleSheet.create({
    wrapper: {
        padding: 10,
    },
});

export const AbstractList = ({
    data = [],
    viewDetailHandler,
    render,
    refreshHandler,
    refreshing,
}) => {
    return (
        <>
            {data.length ? (
                <View style={styles.wrapper}>
                    <FlatList
                        data={data}
                        renderItem={({ item }) =>
                            render(item, viewDetailHandler)
                        }
                        keyExtractor={(item) => item.id}
                        onRefresh={refreshHandler}
                        refreshing={refreshing}
                    />
                </View>
            ) : (
                <NoData />
            )}
        </>
    );
};
