import React, { useState } from 'react';
import {
    SectionList,
    Text,
    StyleSheet,
    View,
    TextInput,
    TouchableWithoutFeedback,
} from 'react-native';
import {
    Card,
    CardItem,
    Body,
    Input,
    Item,
    Label,
    Button,
    Picker,
} from 'native-base';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const styles = StyleSheet.create({
    sectionHeader: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    btn: {
        marginVertical: 10,
        width: '60%',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    btnText: {
        color: 'white',
        fontSize: 18,
    },
});

export const ServicesSectionList = ({
    data,
    edit = false,
    addServiceType,
    addService,
    removeSection,
    removeService,
    changeTextService,
    changeTextServiceType,
}) => {
    const [newSection, addNewSection] = useState('');
    const [newService, addNewService] = useState('');
    const [newServiceCost, addNewServiceCost] = useState(0);
    const [selectedServiceType, setSelectedServiceType] = useState('');

    const submitNewSectionHandler = () => {
        addNewSection('');
        addServiceType(newSection);
    };

    const submitNewServiceHandler = () => {
        addNewService('');
        addNewServiceCost(0);
        const type = selectedServiceType || data[0]?.title;
        addService(newService, type, newServiceCost);
    };

    return (
        <>
            <SectionList
                sections={data}
                renderItem={({ item }) => (
                    <Card>
                        <CardItem>
                            <Body>
                                {edit ? (
                                    <Input
                                        value={item.name}
                                        onChangeText={(text) => {
                                            changeTextService(
                                                text.trim() !== '' ? text : '',
                                                item.name
                                            );
                                        }}
                                    />
                                ) : (
                                    <Text>{item.name}</Text>
                                )}
                            </Body>
                            <View>
                                <View
                                    style={{
                                        flex: 1,
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                    }}
                                >
                                    {edit ? (
                                        <>
                                            <TextInput
                                                value={`${item.cost.toString()}`}
                                                onChangeText={(text) => {
                                                    const numberText = Number(
                                                        text.trim()
                                                    );

                                                    if (
                                                        typeof numberText ===
                                                            'number' &&
                                                        !Number.isNaN(
                                                            numberText
                                                        )
                                                    ) {
                                                        changeTextService(
                                                            item.name,
                                                            item.name,
                                                            numberText
                                                        );
                                                    } else {
                                                        changeTextService(
                                                            item.name,
                                                            item.name,
                                                            0
                                                        );
                                                    }
                                                }}
                                            />
                                            <Text>$</Text>
                                            <TouchableWithoutFeedback
                                                onPress={() =>
                                                    removeService(item.name)
                                                }
                                            >
                                                <MaterialIcons
                                                    name="close"
                                                    color="red"
                                                    size={25}
                                                />
                                            </TouchableWithoutFeedback>
                                        </>
                                    ) : (
                                        <Text>{item.cost.toString()} $</Text>
                                    )}
                                </View>
                            </View>
                        </CardItem>
                    </Card>
                )}
                renderSectionHeader={({ section }) => (
                    <>
                        {edit ? (
                            <View
                                style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}
                            >
                                <Input
                                    value={section.title}
                                    onChangeText={(text) => {
                                        changeTextServiceType(
                                            text.trim() !== '' ? text : '',
                                            section.title
                                        );
                                    }}
                                />
                                <TouchableWithoutFeedback
                                    onPress={() => removeSection(section.title)}
                                >
                                    <MaterialIcons
                                        name="close"
                                        color="red"
                                        size={25}
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                        ) : (
                            <Text style={styles.sectionHeader}>
                                {section.title}
                            </Text>
                        )}
                    </>
                )}
                keyExtractor={(item, index) => index}
            />
            {edit && (
                <>
                    <View>
                        <Item stackedLabel>
                            <Label>Service type name</Label>
                            <Input
                                value={newSection}
                                onChangeText={(text) =>
                                    addNewSection(
                                        text.trim() !== '' ? text : ''
                                    )
                                }
                            />
                        </Item>
                        <Button
                            primary
                            style={styles.btn}
                            onPress={submitNewSectionHandler}
                            disabled={!newSection.length}
                        >
                            <Text style={styles.btnText}>Add service type</Text>
                        </Button>
                    </View>
                    {!!data.length && (
                        <View>
                            <View>
                                <Item picker>
                                    <Picker
                                        mode="dropdown"
                                        placeholder="Choose service type"
                                        selectedValue={selectedServiceType}
                                        onValueChange={setSelectedServiceType}
                                        style={{ width: undefined }}
                                    >
                                        {data.map((serviceType) => (
                                            <Picker.Item
                                                label={serviceType.title}
                                                value={serviceType.title}
                                            />
                                        ))}
                                    </Picker>
                                </Item>
                                <Item stackedLabel>
                                    <Label>Add service</Label>
                                    <Input
                                        value={newService}
                                        onChangeText={(text) => {
                                            addNewService(
                                                text.trim() !== '' ? text : ''
                                            );
                                        }}
                                    />
                                </Item>
                                <Item stackedLabel>
                                    <Label>Add service cost</Label>
                                    <Input
                                        value={newServiceCost}
                                        onChangeText={(text) => {
                                            const numberText = Number(
                                                text.trim()
                                            );

                                            if (
                                                typeof numberText ===
                                                    'number' &&
                                                !Number.isNaN(numberText)
                                            ) {
                                                addNewServiceCost(numberText);
                                            } else {
                                                addNewServiceCost(0);
                                            }
                                        }}
                                    />
                                </Item>
                            </View>
                            <Button
                                primary
                                style={styles.btn}
                                onPress={submitNewServiceHandler}
                                disabled={!newService.length}
                            >
                                <Text style={styles.btnText}>Add service</Text>
                            </Button>
                        </View>
                    )}
                </>
            )}
        </>
    );
};
