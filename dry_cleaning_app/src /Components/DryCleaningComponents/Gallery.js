import React, { useState } from 'react';
import {
    Modal,
    View,
    Text,
    Image,
    StyleSheet,
    TouchableWithoutFeedback,
    Dimensions,
    ImageBackground,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ImagePicker from 'react-native-image-picker';
import { nanoid } from 'nanoid';
import { Button, Input, Item, Label } from 'native-base';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        flexWrap: 'wrap',
    },
    imageWrapper: {
        margin: 2,
        padding: 2,
    },
    image: {
        height: Dimensions.get('window').height / 4 - 12,
        width: Dimensions.get('window').width / 2 - 20,
    },
    modal: {
        flex: 1,
        padding: 40,
        backgroundColor: 'rgba(0,0,0, .9)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: '#fff',
    },
    innerModalWrapper: {
        flex: 1,
    },
    editableImage: {
        alignItems: 'flex-end',
    },
    btnItem: {
        width: '40%',
        justifyContent: 'center',
    },
    btnText: {
        fontSize: 14,
    },
    photoBtnWrapper: {
        flex: 1,
        alignItems: 'center',
        marginVertical: 5,
    },
});

export const Gallery = ({
    photos,
    edit = false,
    removeHandler,
    choosePhotoHandler,
}) => {
    const [modalVisible, setModalVisible] = useState(false);
    const [modalImage, setModalImage] = useState('');
    const [url, setUrl] = useState('');

    const touchImageHandler = (visible, photoId) => {
        if (photoId) {
            const photoItem = photos.find((item) => item.id === photoId);
            setModalImage(photoItem.photo);
        }

        setModalVisible(visible);
    };

    const createNewImage = ({ uri }) => {
        const photo = {
            id: nanoid(),
            photo: uri,
        };

        choosePhotoHandler(photo);
    };

    const addUrlBtnHandler = () => {
        createNewImage({ uri: url });
        setUrl('');
    };

    const choosePhotoBtnHandler = () => {
        const options = {
            title: 'Select Image',
            customButton: [{ name: 'customOptionKey', title: 'Choose Photo' }],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker error: ', response.error);
            } else {
                const source = {
                    uri: response.uri,
                };

                createNewImage(source);
            }
        });
    };

    return (
        <>
            <View style={styles.container}>
                <Modal
                    style={styles.modal}
                    animatedType="fade"
                    transparent
                    visible={modalVisible}
                >
                    <View style={styles.modal}>
                        <Text
                            style={styles.text}
                            onPress={() => touchImageHandler(false)}
                        >
                            Close
                        </Text>
                        <Image
                            style={styles.image}
                            source={{ uri: modalImage }}
                        />
                    </View>
                </Modal>

                {!edit ? (
                    <>
                        {photos.map((item) => (
                            <TouchableWithoutFeedback
                                key={item.id}
                                onPress={() => touchImageHandler(true, item.id)}
                            >
                                <View style={styles.imageWrapper}>
                                    <Image
                                        source={{ uri: item.photo }}
                                        style={styles.image}
                                    />
                                </View>
                            </TouchableWithoutFeedback>
                        ))}
                    </>
                ) : (
                    <>
                        {photos.map((item) => (
                            <TouchableWithoutFeedback
                                key={item.id}
                                onPress={() => touchImageHandler(true, item.id)}
                            >
                                <View style={styles.imageWrapper}>
                                    <ImageBackground
                                        source={{ uri: item.photo }}
                                        style={{
                                            ...styles.image,
                                            ...styles.editableImage,
                                        }}
                                    >
                                        <TouchableWithoutFeedback
                                            onPress={() =>
                                                removeHandler(item.id)
                                            }
                                        >
                                            <MaterialIcons
                                                name="close"
                                                color="red"
                                                size={25}
                                                style={{
                                                    margin: 5,
                                                }}
                                            />
                                        </TouchableWithoutFeedback>
                                    </ImageBackground>
                                </View>
                            </TouchableWithoutFeedback>
                        ))}
                    </>
                )}
            </View>
            {edit && (
                <>
                    <View style={styles.photoBtnWrapper}>
                        <Button
                            bordered
                            style={styles.btnItem}
                            onPress={choosePhotoBtnHandler}
                        >
                            <Text style={styles.btnText}>Choose photo</Text>
                        </Button>
                    </View>
                    <View style={{ marginVertical: 10 }}>
                        <Item stackedLabel>
                            <Label>Photo url</Label>
                            <Input value={url} onChangeText={setUrl} />
                        </Item>
                        <Button
                            bordered
                            style={{
                                ...styles.btnItem,
                                alignSelf: 'center',
                                marginTop: 10,
                            }}
                            onPress={addUrlBtnHandler}
                            disabled={!url.length}
                        >
                            <Text style={styles.btnText}>Add photo url</Text>
                        </Button>
                    </View>
                </>
            )}
        </>
    );
};
