import React from 'react';
import { Image, Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { Card, CardItem, Body, Left } from 'native-base';

const styles = StyleSheet.create({
    cardImage: {
        height: 200,
        width: null,
        flex: 1,
    },
});

export const ListItem = ({ itemData, viewDetailHandler }) => {
    return (
        <TouchableOpacity
            activiteOpacity={0.6}
            onPress={() => viewDetailHandler(itemData.id, itemData.name)}
        >
            <View>
                <Card>
                    <CardItem>
                        <Left>
                            <Body>
                                <Text>{itemData.name}</Text>
                            </Body>
                        </Left>
                    </CardItem>
                    <CardItem>
                        <Image
                            source={{ uri: itemData?.photos[0]?.photo }}
                            style={styles.cardImage}
                        />
                    </CardItem>
                </Card>
            </View>
        </TouchableOpacity>
    );
};
