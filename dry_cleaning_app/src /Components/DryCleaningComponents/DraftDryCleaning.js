import React, { useState } from 'react';
import { View, StyleSheet, Text, ScrollView } from 'react-native';
import { Button, Item, Input, Label, Textarea } from 'native-base';

import { ServicesSectionList } from './ServicesSectionList';
import { Gallery } from './Gallery';

const styles = StyleSheet.create({
    wrapper: {
        margin: 10,
    },
    infoText: {
        fontSize: 18,
        marginVertical: 5,
    },
    btn: {
        marginVertical: 10,
        width: '60%',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    btnText: {
        color: 'white',
        fontSize: 18,
    },
});

export const DraftDryCleaning = ({ data = [], submitEditHandler }) => {
    const [name, setName] = useState(data?.name ?? '');
    const [description, setDescription] = useState(data?.description ?? '');
    const [dryCleaningServices, setDryCleaningServices] = useState(
        data?.dryCleaningServices ?? []
    );
    const [photos, setPhotos] = useState(data?.photos ?? []);

    const isDisabled = () => {
        let disabled = false;

        const isHaveServices = dryCleaningServices.every((item) => {
            const isServiceData = item.data.every(
                (service) =>
                    !!service.name &&
                    (!!Number(service.cost) || Number(service.cost) === 0) &&
                    service.cost !== ''
            );

            return isServiceData && !!item.title.length && !!item.data.length;
        });

        if (!isHaveServices || !name.trim() || !dryCleaningServices.length) {
            disabled = true;
        }
        return disabled;
    };

    const submitDraftHandler = () => {
        submitEditHandler({
            name,
            description,
            serviceTypes: dryCleaningServices.map((item) => ({
                serviceType: item.title,
                services: item.data.map((serviceItem) => ({
                    name: serviceItem.name,
                    cost: serviceItem.cost,
                })),
            })),
            photos: photos.map((photoItem) => ({
                photo: photoItem.photo,
            })),
        });
    };

    const addServiceType = (serviceTypeName) => {
        const serviceTypeOld = dryCleaningServices.find(
            (item) => item.title === serviceTypeName
        );

        if (!serviceTypeOld) {
            setDryCleaningServices((prevState) => [
                ...prevState,
                { title: serviceTypeName, data: [] },
            ]);
        }
    };

    const addService = (serviceName, serviceType, cost) => {
        const oldServiceFinded = dryCleaningServices.find((item) =>
            item.data.find((service) => service.name === serviceName)
        );

        if (!oldServiceFinded) {
            const serviceTypeGroup = dryCleaningServices.find(
                (item) => item.title === serviceType
            );

            const serviceTypeGroups = dryCleaningServices.filter(
                (item) => item.title !== serviceType
            );

            setDryCleaningServices(() => [
                ...serviceTypeGroups,
                {
                    title: serviceTypeGroup.title,
                    data: [
                        ...serviceTypeGroup.data,
                        { name: serviceName, cost },
                    ],
                },
            ]);
        }
    };

    const removeSectionHandler = (sectionTitle) => {
        setDryCleaningServices((prevState) =>
            prevState.filter((item) => item.title !== sectionTitle)
        );
    };

    const removeServiceItemHandler = (serviceItemName) => {
        setDryCleaningServices((prevState) =>
            prevState.map((item) => ({
                ...item,
                data: item.data.filter(
                    (serviceItem) => serviceItem.name !== serviceItemName
                ),
            }))
        );
    };

    const changeTextServiceTypeHandler = (serviceTypeText, serviceTypeName) => {
        const serviceTypeIndex = dryCleaningServices.findIndex(
            (item) => item.title === serviceTypeName
        );

        if (serviceTypeIndex !== -1) {
            setDryCleaningServices((prevState) => {
                const state = [...prevState];
                state[serviceTypeIndex] = {
                    ...state[serviceTypeIndex],
                    title: serviceTypeText,
                };
                return state;
            });
        }
    };

    const changeTextServiceHandler = (serviceText, serviceName, cost) => {
        let serviceIndex;

        const serviceTypeIndex = dryCleaningServices.findIndex((item) => {
            const foundedServiceIndex = item.data.findIndex(
                (serviceItem) => serviceItem.name === serviceName
            );

            if (foundedServiceIndex !== -1) {
                serviceIndex = foundedServiceIndex;
                return true;
            }

            return false;
        });

        if (typeof serviceIndex === 'number') {
            setDryCleaningServices((prevState) => {
                const state = [...prevState];
                const newServices = state[serviceTypeIndex].data.map(
                    (item, index) => {
                        if (index === serviceIndex) {
                            return {
                                name: serviceText,
                                cost: cost ?? item.cost,
                            };
                        }

                        return item;
                    }
                );
                state[serviceTypeIndex] = {
                    ...state[serviceTypeIndex],
                    data: newServices,
                };
                return state;
            });
        }
    };

    const removePhotoHandler = (id) => {
        setPhotos((prevState) => prevState.filter((item) => item.id !== id));
    };

    const choosePhotoHandler = (photo) => {
        setPhotos((preState) => [...preState, photo]);
    };

    return (
        <ScrollView>
            <View style={styles.wrapper}>
                <View>
                    <Item stackedLabel>
                        <Label>Dry cleaning name *</Label>
                        <Input value={name} onChangeText={setName} />
                    </Item>
                    <View>
                        <Text>Dry cleaning description</Text>
                        <Textarea
                            rowSpan={5}
                            bordered
                            value={description}
                            placeholder="Dry cleaning description"
                            onChangeText={setDescription}
                        />
                    </View>
                </View>
                <View>
                    <ServicesSectionList
                        data={dryCleaningServices}
                        edit
                        addServiceType={addServiceType}
                        addService={addService}
                        removeSection={removeSectionHandler}
                        removeService={removeServiceItemHandler}
                        changeTextService={changeTextServiceHandler}
                        changeTextServiceType={changeTextServiceTypeHandler}
                    />
                </View>
                <Gallery
                    photos={photos}
                    edit
                    removeHandler={removePhotoHandler}
                    choosePhotoHandler={choosePhotoHandler}
                />
                <Button
                    primary
                    style={styles.btn}
                    onPress={submitDraftHandler}
                    disabled={isDisabled()}
                >
                    <Text style={styles.btnText}>Submit</Text>
                </Button>
            </View>
        </ScrollView>
    );
};
