import React, { useState } from 'react';
import { View, StyleSheet, Text, ScrollView, Alert } from 'react-native';
import { Button } from 'native-base';

import { useDispatch } from 'react-redux';
import { ServicesSectionList } from './ServicesSectionList';
import { Gallery } from './Gallery';
import { NoData } from '../NoData';
import { ClientServicesSectionList } from './ClientServiceSectionList';
import {
    backLocalUserMoneyAction,
    subtractLocalUserMoneyAction,
} from '../../store/actions/userAction';
import { useAuth } from '../../hooks/useAuth';

const styles = StyleSheet.create({
    wrapper: {
        margin: 10,
    },
    infoText: {
        fontSize: 18,
        marginVertical: 5,
    },
    btn: {
        marginVertical: 10,
        width: '40%',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    btnText: {
        color: 'white',
        fontSize: 18,
    },
    btnGroupWrapper: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
});

export const ListItemDetail = ({
    data = {},
    editHandler,
    removeHandler,
    createOrder,
}) => {
    const { user } = useAuth();
    const dispatch = useDispatch();
    const [selectedServices, changeSelectedServices] = useState([]);

    const toggleService = (item) => {
        const service = selectedServices.find(
            (serviceItem) => serviceItem.name === item.name
        );

        if (service) {
            changeSelectedServices((prevState) =>
                prevState.filter(
                    (serviceItem) => serviceItem.name !== service.name
                )
            );
            dispatch(backLocalUserMoneyAction(item.cost));
        } else if (user.money >= item.cost) {
            changeSelectedServices((prevState) => [
                ...prevState,
                { id: item.id, name: item.name, cost: item.cost },
            ]);
            dispatch(subtractLocalUserMoneyAction(item.cost));
        } else {
            Alert.alert(
                'Not enough money',
                `You have ${user.money} $, but service cost is ${item.cost} $`,
                [
                    {
                        text: 'OK',
                    },
                ]
            );
        }
    };

    return (
        <>
            {Object.keys(data).length ? (
                <ScrollView>
                    <View style={styles.wrapper}>
                        <View>
                            <Text style={styles.infoText}>{data.name}</Text>
                            <Text style={styles.infoText}>
                                {data.description}
                            </Text>
                        </View>
                        <View>
                            {user.role === 'admin' ? (
                                <ServicesSectionList
                                    data={data.dryCleaningServices}
                                    edit={false}
                                />
                            ) : (
                                <ClientServicesSectionList
                                    data={data.dryCleaningServices}
                                    toggleService={toggleService}
                                    selectedServices={selectedServices}
                                    user={user}
                                />
                            )}
                        </View>
                        <Gallery photos={data.photos} />
                        <View style={styles.btnGroupWrapper}>
                            {user.role === 'admin' ? (
                                <>
                                    <Button
                                        primary
                                        style={styles.btn}
                                        onPress={editHandler}
                                    >
                                        <Text style={styles.btnText}>Edit</Text>
                                    </Button>
                                    <Button
                                        primary
                                        onPress={removeHandler}
                                        style={styles.btn}
                                    >
                                        <Text style={styles.btnText}>
                                            Delete
                                        </Text>
                                    </Button>
                                </>
                            ) : (
                                <Button
                                    primary
                                    style={styles.btn}
                                    onPress={() =>
                                        createOrder(selectedServices)
                                    }
                                    disabled={!selectedServices.length}
                                >
                                    <Text style={styles.btnText}>
                                        Create order
                                    </Text>
                                </Button>
                            )}
                        </View>
                    </View>
                </ScrollView>
            ) : (
                <NoData />
            )}
        </>
    );
};
