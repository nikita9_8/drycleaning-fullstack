import React from 'react';
import { StyleSheet, FlatList, View } from 'react-native';

import { NoData } from '../NoData';
import { ListItem } from './ListItem';

const styles = StyleSheet.create({
    wrapper: {
        padding: 10,
    },
});

export const List = ({ data = [], viewDetailHandler }) => {
    return (
        <>
            {data.length ? (
                <View style={styles.wrapper}>
                    <FlatList
                        data={data}
                        renderItem={({ item }) => (
                            <ListItem
                                itemData={item}
                                viewDetailHandler={viewDetailHandler}
                            />
                        )}
                        keyExtractor={(item) => item.id}
                    />
                </View>
            ) : (
                <NoData />
            )}
        </>
    );
};
