import React from 'react';
import {
    SectionList,
    Text,
    StyleSheet,
    View,
    TouchableOpacity,
} from 'react-native';
import { Card, CardItem, Body } from 'native-base';

const styles = StyleSheet.create({
    sectionHeader: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    btn: {
        marginVertical: 10,
        width: '60%',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    btnText: {
        color: 'white',
        fontSize: 18,
    },
});

export const ClientServicesSectionList = ({
    data,
    toggleService,
    selectedServices,
    user,
}) => {
    const styleItem = (item) => {
        let style =
            user.money >= item.cost
                ? { backgroundColor: 'green' }
                : { backgroundColor: 'red' };

        if (selectedServices.length) {
            const service = selectedServices.find(
                (serviceItem) => serviceItem.name === item.name
            );

            if (service) {
                style = { backgroundColor: 'yellow' };
            }
        }

        return style;
    };

    return (
        <>
            <SectionList
                sections={data}
                renderItem={({ item }) => (
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => toggleService(item)}
                    >
                        <Card>
                            <CardItem style={styleItem(item)}>
                                <Body>
                                    <Text>{item.name}</Text>
                                </Body>
                                <View>
                                    <View
                                        style={{
                                            flex: 1,
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                        }}
                                    >
                                        <Text>{item.cost.toString()} $</Text>
                                    </View>
                                </View>
                            </CardItem>
                        </Card>
                    </TouchableOpacity>
                )}
                renderSectionHeader={({ section }) => (
                    <>
                        <Text style={styles.sectionHeader}>
                            {section.title}
                        </Text>
                    </>
                )}
                keyExtractor={(item, index) => index}
            />
        </>
    );
};
