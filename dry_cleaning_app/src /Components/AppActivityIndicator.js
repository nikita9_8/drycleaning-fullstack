import React from 'react';
import { StyleSheet, ActivityIndicator, View } from 'react-native';

const styles = StyleSheet.create({
    activityIndicatorWrapper: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export const AppActivityIndicator = () => {
    return (
        <View style={styles.activityIndicatorWrapper}>
            <ActivityIndicator size="large" />
        </View>
    );
};
