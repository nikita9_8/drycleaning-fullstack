import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useAuth } from '../hooks/useAuth';

export const HeaderRightCustom = () => {
    const { logout } = useAuth();

    return (
        <TouchableWithoutFeedback onPress={logout}>
            <MaterialIcons
                name="keyboard-return"
                size={25}
                style={{ marginRight: 10 }}
            />
        </TouchableWithoutFeedback>
    );
};
