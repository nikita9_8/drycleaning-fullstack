import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, ScrollView } from 'react-native';
import { Button, Item, Input, Label, Textarea, Picker } from 'native-base';
import moment from 'moment';
import DatePicker from 'react-native-date-picker';

import { ServicesSectionList } from '../DryCleaningComponents/ServicesSectionList';

const styles = StyleSheet.create({
    wrapper: {
        margin: 10,
    },
    infoText: {
        fontSize: 18,
        marginVertical: 5,
    },
    btn: {
        marginVertical: 10,
        width: '60%',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    btnText: {
        color: 'white',
        fontSize: 18,
    },
    commentBlock: {
        borderWidth: 1,
        padding: 5,
        marginVertical: 10,
    },
    title: {
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    center: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export const DraftOrder = ({ data = [], submitEditHandler }) => {
    const [userName, setUserName] = useState(data?.user_info ?? '');
    const [status, setStatus] = useState(data?.status ?? '');
    const [createdAt, setCreatedAt] = useState(
        data?.created_at ? new Date(data.created_at) : ''
    );
    const [comment, setComment] = useState(data?.comment ?? '');
    const [dryCleaningServices, setDryCleaningServices] = useState(
        data?.dryCleaningServices ?? []
    );
    const [orderCost, setOrderCost] = useState(data?.cost ?? 0);

    useEffect(() => {
        if (status !== 'return') {
            setComment('');
        }
    }, [status]);

    const submitDraftHandler = () => {
        submitEditHandler({
            user_info: userName,
            created_at: moment(createdAt).format('YYYY-MM-DD hh:mm:ss'),
            servicesGroup: dryCleaningServices.map((item) => ({
                serviceType: item.title,
                services: item.data.map((serviceItem) => ({
                    name: serviceItem.name,
                    cost: serviceItem.cost,
                })),
            })),
            status,
            comment,
        });
    };

    const isDisabled = () => {
        let disabled = false;

        const isHaveServices = dryCleaningServices.every((item) => {
            const isServiceData = item.data.every(
                (service) =>
                    !!service.name &&
                    (!!Number(service.cost) || Number(service.cost) === 0) &&
                    service.cost !== ''
            );

            return isServiceData && !!item.title.length && !!item.data.length;
        });
        // !createdAt.match(
        //     '^20[0-9]{2}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$'
        // )

        if (
            !isHaveServices ||
            !userName.trim() ||
            !dryCleaningServices.length ||
            !status.length ||
            !createdAt ||
            (status === 'return' && !comment.trim().length)
        ) {
            disabled = true;
        }
        return disabled;
    };

    const addServiceType = (serviceTypeName) => {
        const serviceTypeOld = dryCleaningServices.find(
            (item) => item.title === serviceTypeName
        );

        if (!serviceTypeOld) {
            setDryCleaningServices((prevState) => [
                ...prevState,
                { title: serviceTypeName, data: [] },
            ]);
        }
    };

    const addService = (serviceName, serviceType, cost) => {
        const oldServiceFinded = dryCleaningServices.find((item) =>
            item.data.find((service) => service.name === serviceName)
        );

        if (!oldServiceFinded) {
            const serviceTypeGroup = dryCleaningServices.find(
                (item) => item.title === serviceType
            );

            const serviceTypeGroups = dryCleaningServices.filter(
                (item) => item.title !== serviceType
            );

            setDryCleaningServices(() => [
                ...serviceTypeGroups,
                {
                    title: serviceTypeGroup.title,
                    data: [
                        ...serviceTypeGroup.data,
                        { name: serviceName, cost },
                    ],
                },
            ]);
        }
    };

    const removeSectionHandler = (sectionTitle) => {
        setDryCleaningServices((prevState) =>
            prevState.filter((item) => item.title !== sectionTitle)
        );
    };

    const removeServiceItemHandler = (serviceItemName) => {
        setDryCleaningServices((prevState) =>
            prevState.map((item) => ({
                ...item,
                data: item.data.filter(
                    (serviceItem) => serviceItem.name !== serviceItemName
                ),
            }))
        );
    };

    const changeTextServiceTypeHandler = (serviceTypeText, serviceTypeName) => {
        const serviceTypeIndex = dryCleaningServices.findIndex(
            (item) => item.title === serviceTypeName
        );

        if (serviceTypeIndex !== -1) {
            setDryCleaningServices((prevState) => {
                const state = [...prevState];
                state[serviceTypeIndex] = {
                    ...state[serviceTypeIndex],
                    title: serviceTypeText,
                };
                return state;
            });
        }
    };

    const changeTextServiceHandler = (serviceText, serviceName, cost) => {
        let serviceIndex;

        const serviceTypeIndex = dryCleaningServices.findIndex((item) => {
            const foundedServiceIndex = item.data.findIndex(
                (serviceItem) => serviceItem.name === serviceName
            );

            if (foundedServiceIndex !== -1) {
                serviceIndex = foundedServiceIndex;
                return true;
            }

            return false;
        });

        if (typeof serviceIndex === 'number') {
            setDryCleaningServices((prevState) => {
                const state = [...prevState];
                const newServices = state[serviceTypeIndex].data.map(
                    (item, index) => {
                        if (index === serviceIndex) {
                            return {
                                name: serviceText,
                                cost: cost ?? item.cost,
                            };
                        }

                        return item;
                    }
                );
                state[serviceTypeIndex] = {
                    ...state[serviceTypeIndex],
                    data: newServices,
                };

                return state;
            });
        }
    };

    useEffect(() => {
        let newCost = 0;

        dryCleaningServices.forEach((item) =>
            item.data.forEach((serviceItem) => {
                newCost += Number(serviceItem.cost);
            })
        );

        setOrderCost(newCost);
    }, [dryCleaningServices]);

    return (
        <ScrollView>
            <View style={styles.wrapper}>
                <View>
                    <Item stackedLabel>
                        <Label>UserName</Label>
                        <Input value={userName} onChangeText={setUserName} />
                    </Item>
                    <Text style={styles.infoText}>Change Created at date</Text>
                    <View style={styles.center}>
                        <DatePicker
                            date={createdAt}
                            onDateChange={setCreatedAt}
                        />
                    </View>
                    {/* <Item stackedLabel>
                        <Label>Date</Label>
                        <Input value={createdAt} onChangeText={setCreatedAt} />
                    </Item>
                    {!createdAt.match(
                        '^20[0-9]{2}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$'
                    ) && (
                        <Text style={{ ...styles.infoText, color: 'red' }}>
                            You need print date in format YYYY-MM-DD hh:mm:ss
                        </Text>
                    )} */}
                    <Text style={styles.infoText}>Cost: {orderCost}</Text>
                    <Text style={styles.infoText}>
                        Dry cleaning: {data.dryCleaning.name}
                    </Text>
                    <Text style={styles.infoText}>Status</Text>
                    <Picker
                        mode="dropdown"
                        placeholder="Press to select status"
                        selectedValue={status}
                        onValueChange={setStatus}
                    >
                        <Picker.Item
                            label="In precessing"
                            value="in precessing"
                        />
                        <Picker.Item label="Return" value="return" />
                        <Picker.Item
                            label="Ready for extradition"
                            value="ready for extradition"
                        />
                    </Picker>
                    {status === 'return' && (
                        <View style={styles.commentBlock}>
                            <Text style={styles.title}>Return comment</Text>
                            <Textarea
                                rowSpan={5}
                                bordered
                                value={comment}
                                placeholder="Dry cleaning description"
                                onChangeText={setComment}
                            />
                        </View>
                    )}
                </View>
                <View>
                    <ServicesSectionList
                        data={dryCleaningServices}
                        edit
                        addServiceType={addServiceType}
                        addService={addService}
                        removeSection={removeSectionHandler}
                        removeService={removeServiceItemHandler}
                        changeTextService={changeTextServiceHandler}
                        changeTextServiceType={changeTextServiceTypeHandler}
                    />
                </View>
                <Button
                    primary
                    style={styles.btn}
                    onPress={submitDraftHandler}
                    disabled={isDisabled()}
                >
                    <Text style={styles.btnText}>Submit</Text>
                </Button>
            </View>
        </ScrollView>
    );
};
