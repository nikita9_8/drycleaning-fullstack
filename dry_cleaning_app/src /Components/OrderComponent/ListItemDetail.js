import React from 'react';
import { View, StyleSheet, Text, ScrollView } from 'react-native';
import { Button } from 'native-base';

import moment from 'moment';
import { ServicesSectionList } from '../DryCleaningComponents/ServicesSectionList';
import { NoData } from '../NoData';

const styles = StyleSheet.create({
    wrapper: {
        margin: 10,
    },
    infoText: {
        fontSize: 18,
        marginVertical: 5,
    },
    btn: {
        marginVertical: 10,
        width: '40%',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    btnText: {
        color: 'white',
        fontSize: 18,
    },
    btnGroupWrapper: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    commentBlock: {
        borderWidth: 1,
        padding: 5,
        marginVertical: 10,
    },
    title: {
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
    },
});

export const ListItemDetail = ({
    data = {},
    editHandler,
    removeHandler,
    user,
}) => {
    return (
        <>
            {Object.keys(data).length ? (
                <ScrollView>
                    <View style={styles.wrapper}>
                        <View>
                            <Text style={styles.infoText}>
                                UserName: {data.user_info}
                            </Text>
                            <Text style={styles.infoText}>
                                Status: {data.status}
                            </Text>
                            <Text style={styles.infoText}>
                                Cost: {data.cost}
                            </Text>
                            <Text style={styles.infoText}>
                                Dry cleaning: {data.dryCleaning.name}
                            </Text>
                            <Text style={styles.infoText}>
                                {`Order create time: ${moment(
                                    data.created_at
                                ).format('YYYY-MM-DD hh:mm:ss')}`}
                            </Text>
                            {data.comment && (
                                <View style={styles.commentBlock}>
                                    <Text style={styles.title}>
                                        Return comment
                                    </Text>
                                    <Text style={styles.infoText}>
                                        {data.comment}
                                    </Text>
                                </View>
                            )}
                        </View>
                        <View>
                            <ServicesSectionList
                                data={data.dryCleaningServices}
                                edit={false}
                            />
                        </View>
                        <View style={styles.btnGroupWrapper}>
                            {user.role === 'admin' ? (
                                <>
                                    <Button
                                        primary
                                        style={styles.btn}
                                        onPress={editHandler}
                                    >
                                        <Text style={styles.btnText}>Edit</Text>
                                    </Button>
                                    <Button
                                        primary
                                        onPress={removeHandler}
                                        style={styles.btn}
                                    >
                                        <Text style={styles.btnText}>
                                            Delete
                                        </Text>
                                    </Button>
                                </>
                            ) : data.status === 'completed' ? (
                                <Button
                                    primary
                                    onPress={removeHandler}
                                    style={styles.btn}
                                >
                                    <Text style={styles.btnText}>Complete</Text>
                                </Button>
                            ) : (
                                <></>
                            )}
                        </View>
                    </View>
                </ScrollView>
            ) : (
                <NoData />
            )}
        </>
    );
};
