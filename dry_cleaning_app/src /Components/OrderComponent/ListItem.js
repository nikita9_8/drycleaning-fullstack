import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { Card, CardItem, Body } from 'native-base';

const styles = StyleSheet.create({
    itemWrapper: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 10,
    },
});

export const ListItem = ({ itemData, viewDetailHandler }) => {
    return (
        <>
            <TouchableOpacity
                activeOpacity={0.2}
                onPress={() => viewDetailHandler(itemData.id)}
            >
                <Card>
                    <CardItem>
                        <Body style={styles.itemWrapper}>
                            <Text>{itemData.user_info}</Text>
                            <Text>{itemData.status}</Text>
                            <Text>{itemData.cost} $</Text>
                        </Body>
                    </CardItem>
                </Card>
            </TouchableOpacity>
        </>
    );
};
