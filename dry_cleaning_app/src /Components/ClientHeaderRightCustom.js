import React from 'react';
import { TouchableWithoutFeedback, View, Text, StyleSheet } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useAuth } from '../hooks/useAuth';

const styles = StyleSheet.create({
    headerWrapper: {
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
    },
    text: { marginHorizontal: 10 },
});

export const ClientHeaderRightCustom = () => {
    const { logout, user } = useAuth();

    const moneyBalance =
        user.money >= 0 ? `Balance ${user.money} $` : "Update you'r balance";

    return (
        <View style={styles.headerWrapper}>
            <Text style={styles.text}>{moneyBalance}</Text>
            <TouchableWithoutFeedback onPress={logout}>
                <MaterialIcons
                    name="keyboard-return"
                    size={25}
                    style={{ marginRight: 10 }}
                />
            </TouchableWithoutFeedback>
        </View>
    );
};
