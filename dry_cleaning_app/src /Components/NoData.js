import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

const styles = StyleSheet.create({
    noDataWrapper: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        fontSize: 20,
    },
});

export const NoData = () => {
    return (
        <View style={styles.noDataWrapper}>
            <Text style={styles.text}>No data</Text>
        </View>
    );
};
