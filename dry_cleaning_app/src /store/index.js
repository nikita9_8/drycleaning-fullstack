import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import { userReducer } from './reducers/user';
import { orderReducer } from './reducers/order';
import { dryCleaningReducer } from './reducers/dryCleaning';

const store = combineReducers({
    user: userReducer,
    order: orderReducer,
    dryCleaning: dryCleaningReducer,
});

export default createStore(store, applyMiddleware(thunk));
