import axios from 'axios';

import FormData from 'form-data';
import {
    ORDER_DELETE,
    ORDER_LIST,
    ORDER_LOADED,
    ORDER_LOADING,
    ORDER_UPDATE,
    ORDER_CREATE,
} from '../types';

import { ServiceTypeServiceRelation } from '../../utils/serviceTypeServiceRelation';

export const createOrderAction = (
    { token },
    serviceTypeGroups,
    dryCleaningId
) => async (dispatch) => {
    try {
        dispatch({
            type: ORDER_LOADING,
        });
        const config = {
            headers: { Authorization: `Bearer ${token}` },
        };

        const form = new FormData();
        form.append('dry_cleaning_id', dryCleaningId);
        form.append('serviceGroup', JSON.stringify(serviceTypeGroups));
        console.log(form);
        const { data } = await axios.post(
            `http://localhost:3333/api/orders/create`,
            form,
            config
        );

        dispatch({
            type: ORDER_CREATE,
            payload: data,
        });
    } catch (e) {
        console.log(e);
        dispatch({
            type: ORDER_LOADED,
        });
    }
};

export const deleteOrderAction = ({ token }, orderId) => async (dispatch) => {
    try {
        dispatch({
            type: ORDER_LOADING,
        });
        const config = {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        };

        await axios.delete(
            `http://localhost:3333/api/orders/${orderId}`,
            config
        );

        dispatch({
            type: ORDER_DELETE,
            payload: orderId,
        });
    } catch (e) {
        console.log(e);
        dispatch({
            type: ORDER_LOADED,
        });
    }
};

export const allOrdersAction = ({ token }, role) => async (dispatch) => {
    try {
        dispatch({
            type: ORDER_LOADING,
        });
        const config = {
            headers: { Authorization: `Bearer ${token}` },
        };

        let { data } = await axios.get(
            `http://localhost:3333/api/orders`,
            config
        );

        data = data.map((item) => {
            let { status } = item;

            if (role === 'admin' && status === 'in precessing') {
                status = 'new';
            } else if (
                role === 'client' &&
                status === 'ready for extradition'
            ) {
                status = 'completed';
            }

            return {
                ...item,
                status,
            };
        });

        data = ServiceTypeServiceRelation(data);

        dispatch({
            type: ORDER_LIST,
            payload: data,
        });
    } catch (e) {
        console.log(e);
        dispatch({
            type: ORDER_LOADED,
        });
    }
};

export const orderPutAction = ({ token }, order) => async (dispatch) => {
    try {
        dispatch({
            type: ORDER_LOADING,
        });
        const config = {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        };

        const orderForm = new FormData();
        orderForm.append('user_info', order.user_info);
        orderForm.append('created_at', order.created_at);
        orderForm.append('status', order.status);
        orderForm.append('comment', order.comment);
        orderForm.append('servicesGroup', JSON.stringify(order.servicesGroup));

        let { data } = await axios.put(
            `http://localhost:3333/api/orders/${order.id}`,
            orderForm,
            config
        );

        data = ServiceTypeServiceRelation([data]);

        dispatch({
            type: ORDER_UPDATE,
            payload: data[0],
        });
    } catch (e) {
        console.log(e);
        dispatch({
            type: ORDER_LOADED,
        });
    }
};
