import axios from 'axios';
import FormData from 'form-data';
import AsyncStorage from '@react-native-community/async-storage';

import {
    USER_LOGIN,
    USER_LOGOUT,
    USER_RESTORE,
    LOADING,
    LOADED,
    USER_REGISTER,
    ALL_CLIENTS,
    SUBTRACT_MONEY,
    BACK_MONEY,
} from '../types';

const loginRestoreHelper = async ({ email, password }, request) => {
    const form = new FormData();
    form.append('email', email);
    form.append('password', password);

    const { data } = await axios.post(
        `http://localhost:3333/api/${request}`,
        form
    );
    await AsyncStorage.setItem('user', JSON.stringify(data));

    return data;
};

export const userLoginAction = (data) => async (dispatch) => {
    try {
        dispatch({
            type: LOADING,
        });

        const requestData = await loginRestoreHelper(data, 'login');

        dispatch({
            type: USER_LOGIN,
            payload: requestData,
        });
    } catch (e) {
        dispatch({
            type: LOADED,
        });
        console.log(e);
    }
};

export const userLogoutAction = ({ token }) => async (dispatch) => {
    try {
        const config = {
            headers: { Authorization: `Bearer ${token}` },
        };

        await AsyncStorage.removeItem('user');

        const { data } = await axios.get(
            `http://localhost:3333/api/logout`,
            config
        );

        if (data) {
            dispatch({
                type: USER_LOGOUT,
            });
        }
    } catch (e) {
        console.log(e);
        dispatch({
            type: LOADED,
        });
    }
};

export const userRegisterAction = ({
    email,
    password,
    username,
    userRole,
}) => async (dispatch) => {
    try {
        dispatch({
            type: LOADING,
        });

        const form = new FormData();
        form.append('email', email);
        form.append('password', password);
        form.append('username', username);
        form.append('role', userRole);
        form.append('money', Math.floor(Math.random() * 500));

        const { data } = await axios.post(
            `http://localhost:3333/api/register`,
            form
        );
        await AsyncStorage.setItem('user', JSON.stringify(data));

        dispatch({
            type: USER_REGISTER,
            payload: data,
        });
    } catch (e) {
        console.log(e);
        dispatch({
            type: LOADED,
        });
    }
};

export const userRestoreAction = (data) => async (dispatch) => {
    try {
        dispatch({
            type: LOADING,
        });

        await loginRestoreHelper(data, 'restore');

        dispatch({
            type: LOADED,
        });
    } catch (e) {
        dispatch({
            type: LOADED,
        });
        console.log(e);
    }
};

export const allClients = ({ token }) => async (dispatch) => {
    try {
        dispatch({
            type: LOADING,
        });
        const config = {
            headers: { Authorization: `Bearer ${token}` },
        };

        const { data } = await axios.get(
            `http://localhost:3333/api/clients`,
            config
        );

        dispatch({
            type: ALL_CLIENTS,
            payload: data,
        });
    } catch (e) {
        console.log(e);
        dispatch({
            type: LOADED,
        });
    }
};

export const checkUserAction = () => async (dispatch) => {
    try {
        dispatch({
            type: LOADING,
        });

        let value = await AsyncStorage.getItem('user');
        value = JSON.parse(value);

        const config = {
            headers: { Authorization: `Bearer ${value?.token?.token}` },
        };

        const { data } = await axios.get(
            `http://localhost:3333/api/users/${value.id}`,
            config
        );

        data.token = value.token;

        dispatch({
            type: USER_LOGIN,
            payload: data,
        });
    } catch (e) {
        console.log(e);
        dispatch({
            type: LOADED,
        });
    }
};

export const backLocalUserMoneyAction = (cost) => async (dispatch) => {
    dispatch({
        type: BACK_MONEY,
        payload: cost,
    });
};

export const subtractLocalUserMoneyAction = (cost) => async (dispatch) => {
    dispatch({
        type: SUBTRACT_MONEY,
        payload: cost,
    });
};
