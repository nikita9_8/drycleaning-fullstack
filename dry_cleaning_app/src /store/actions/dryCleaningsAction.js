import axios from 'axios';
import FormData from 'form-data';

import {
    DRY_CLEANING_DELETE,
    DRY_CLEANING_LIST,
    DRY_CLEANING_LOADED,
    DRY_CLEANING_LOADING,
    DRY_CLEANING_CREATE,
    DRY_CLEANING_UPDATE,
} from '../types';

import { ServiceTypeServiceRelation } from '../../utils/serviceTypeServiceRelation';

export const dryCleaningListAction = ({ token }) => async (dispatch) => {
    try {
        const config = {
            headers: { Authorization: `Bearer ${token}` },
        };

        let { data } = await axios.get(
            `http://localhost:3333/api/drycleanings`,
            config
        );

        data = data.map((item) =>
            item.dryCleaningServices.length
                ? ServiceTypeServiceRelation([item])[0]
                : item
        );

        dispatch({
            type: DRY_CLEANING_LIST,
            payload: data,
        });
    } catch (e) {
        console.log(e);
        dispatch({
            type: DRY_CLEANING_LOADED,
        });
    }
};

export const dryCleaningDeleteAction = (dryCleaningId, { token }) => async (
    dispatch
) => {
    try {
        dispatch({
            type: DRY_CLEANING_LOADING,
        });

        const config = {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        };

        await axios.delete(
            `http://localhost:3333/api/drycleanings/${dryCleaningId}`,
            config
        );

        dispatch({
            type: DRY_CLEANING_DELETE,
            payload: dryCleaningId,
        });
    } catch (e) {
        console.log(e);
        dispatch({
            type: DRY_CLEANING_LOADED,
        });
    }
};

export const dryCleaningCreateAction = (dryCleaning, { token }) => async (
    dispatch
) => {
    try {
        dispatch({
            type: DRY_CLEANING_LOADING,
        });
        const config = {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        };

        const dryCleaningForm = new FormData();
        dryCleaningForm.append('name', dryCleaning.name);
        dryCleaningForm.append('description', dryCleaning.description);
        dryCleaningForm.append(
            'serviceTypes',
            JSON.stringify(dryCleaning.serviceTypes)
        );
        dryCleaningForm.append('photos', JSON.stringify(dryCleaning.photos));

        let { data } = await axios.post(
            `http://localhost:3333/api/drycleanings/create`,
            dryCleaningForm,
            config
        );

        data = ServiceTypeServiceRelation(data);

        dispatch({
            type: DRY_CLEANING_CREATE,
            payload: data[0],
        });
    } catch (e) {
        console.log(e);
        dispatch({
            type: DRY_CLEANING_LOADED,
        });
    }
};

export const dryCleaningPutAction = (dryCleaning, { token }) => async (
    dispatch
) => {
    try {
        dispatch({
            type: DRY_CLEANING_LOADING,
        });
        const config = {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        };

        const dryCleaningForm = new FormData();
        dryCleaningForm.append('name', dryCleaning.name);
        dryCleaningForm.append('description', dryCleaning.description);
        dryCleaningForm.append(
            'serviceTypes',
            JSON.stringify(dryCleaning.serviceTypes)
        );
        dryCleaningForm.append('photos', JSON.stringify(dryCleaning.photos));

        let { data } = await axios.put(
            `http://localhost:3333/api/drycleanings/${dryCleaning.id}`,
            dryCleaningForm,
            config
        );

        data = ServiceTypeServiceRelation(data);

        dispatch({
            type: DRY_CLEANING_UPDATE,
            payload: data[0],
        });
    } catch (e) {
        console.log(e);
        dispatch({
            type: DRY_CLEANING_LOADED,
        });
    }
};
