// Auth types
export const USER_LOGIN = 'USER_LOGIN';
export const USER_RESTORE = 'USER_RESTORE';
export const USER_LOGOUT = 'USER_LOGOUT';
export const USER_REGISTER = 'USER_REGISTER';
export const LOADING = 'LOADING';
export const LOADED = 'LOADED';
export const ALL_CLIENTS = 'ALL_CLIENTS';
export const SUBTRACT_MONEY = 'SUBTRACT_MONEY';
export const BACK_MONEY = 'BACK_MONEY';

// DryCleaning types
export const DRY_CLEANING_LIST = 'DRY_CLEANING_LIST';
export const DRY_CLEANING_LOADING = 'DRY_CLEANING_LOADING';
export const DRY_CLEANING_LOADED = 'DRY_CLEANING_LOADED';
export const DRY_CLEANING_CREATE = 'DRY_CLEANING_CREATE';
export const DRY_CLEANING_UPDATE = 'DRY_CLEANING_UPDATE';
export const DRY_CLEANING_DELETE = 'DRY_CLEANING_DELETE';

// Order types
export const ORDER_LIST = 'ORDER_LIST';
export const ORDER_LOADING = 'ORDER_LOADING';
export const ORDER_LOADED = 'ORDER_LOADED';
export const ORDER_UPDATE = 'ORDER_UPDATE';
export const ORDER_DELETE = 'ORDER_DELETE';
export const ORDER_CREATE = 'ORDER_CREATE';
