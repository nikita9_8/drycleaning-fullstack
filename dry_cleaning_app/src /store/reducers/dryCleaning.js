import produce from 'immer';

import {
    DRY_CLEANING_DELETE,
    DRY_CLEANING_LIST,
    DRY_CLEANING_LOADED,
    DRY_CLEANING_LOADING,
    DRY_CLEANING_CREATE,
    DRY_CLEANING_UPDATE,
} from '../types';

const initialState = {
    dryCleaningList: [],
    loading: false,
};

export const dryCleaningReducer = (state = initialState, action) => {
    return produce(state, (draft) => {
        if (action.type === DRY_CLEANING_LOADING) {
            draft.loading = true;
        } else if (action.type === DRY_CLEANING_LOADED) {
            draft.loading = false;
        } else if (action.type === DRY_CLEANING_LIST) {
            draft.dryCleaningList = action.payload;
            draft.loading = false;
        } else if (action.type === DRY_CLEANING_UPDATE) {
            const dryCleaningIndex = state.dryCleaningList.findIndex(
                (item) => item.id === action.payload.id
            );

            draft.dryCleaningList[dryCleaningIndex] = action.payload;
            draft.loading = false;
        } else if (action.type === DRY_CLEANING_CREATE) {
            draft.dryCleaningList.push(action.payload);
            draft.loading = false;
        } else if (action.type === DRY_CLEANING_DELETE) {
            draft.dryCleaningList = state.dryCleaningList.filter(
                (item) => item.id !== action.payload
            );
            draft.loading = false;
        }
    });
};
