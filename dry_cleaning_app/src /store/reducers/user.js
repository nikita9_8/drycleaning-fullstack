import produce from 'immer';

import {
    USER_LOGIN,
    LOADING,
    USER_RESTORE,
    USER_LOGOUT,
    USER_REGISTER,
    LOADED,
    ALL_CLIENTS,
    SUBTRACT_MONEY,
    BACK_MONEY,
} from '../types';

const initialState = {
    currentUser: {},
    allUsers: [],
    loading: false,
    token: '',
};

export const userReducer = (state = initialState, action) => {
    return produce(state, (draft) => {
        if (action.type === LOADING) {
            draft.loading = true;
        } else if (action.type === LOADED) {
            draft.loading = false;
        } else if (
            action.type === USER_REGISTER ||
            action.type === USER_LOGIN
        ) {
            draft.currentUser = action.payload;
            draft.token = action.payload?.token ?? state.token;
            draft.loading = false;
        } else if (action.type === USER_LOGOUT) {
            draft.currentUser = {};
            draft.token = '';
        } else if (action.type === ALL_CLIENTS) {
            draft.allUsers = action.payload;
            draft.loading = false;
        } else if (action.type === SUBTRACT_MONEY) {
            draft.currentUser.money -= action.payload;
        } else if (action.type === BACK_MONEY) {
            draft.currentUser.money += action.payload;
        }
    });
};
