import produce from 'immer';

import {
    ORDER_DELETE,
    ORDER_LIST,
    ORDER_LOADED,
    ORDER_LOADING,
    ORDER_UPDATE,
    ORDER_CREATE,
    DRY_CLEANING_DELETE,
    DRY_CLEANING_UPDATE,
} from '../types';

const initialState = {
    orders: [],
    loading: false,
};

export const orderReducer = (state = initialState, action) => {
    return produce(state, (draft) => {
        if (action.type === ORDER_LIST) {
            draft.orders = action.payload;
            draft.loading = false;
        } else if (action.type === ORDER_LOADING) {
            draft.loading = true;
        } else if (action.type === ORDER_LOADED) {
            draft.loading = false;
        } else if (action.type === ORDER_CREATE) {
            draft.orders.push(action.payload);
            draft.loading = false;
        } else if (action.type === ORDER_DELETE) {
            draft.orders = state.orders.filter(
                (item) => item.id !== action.payload
            );
            draft.loading = false;
        } else if (action.type === ORDER_UPDATE) {
            const orderIndex = state.orders.findIndex(
                (item) => item.id === action.payload.id
            );

            draft.orders[orderIndex] = action.payload;
            draft.loading = false;
        }
    });
};
