import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { userLogoutAction, checkUserAction } from '../store/actions/userAction';

export const useAuth = () => {
    const dispatch = useDispatch();
    const token = useSelector((state) => state.user.token);
    const isLoading = useSelector((state) => state.user.loading);
    const user = useSelector((state) => state.user.currentUser);

    useEffect(() => {
        dispatch(checkUserAction());
    }, [dispatch]);

    const logout = useCallback(() => {
        dispatch(userLogoutAction(token));
    }, [dispatch, token]);

    return {
        logout,
        isLoading,
        token,
        user,
    };
};
