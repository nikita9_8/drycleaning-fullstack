import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';

import { DryCleaningList } from '../Screens/AdminScreens/DryCleaningScreens/DryCleaningList';
import { DryCleaning } from '../Screens/AdminScreens/DryCleaningScreens/DryCleaning';
import { EditDryCleaning } from '../Screens/AdminScreens/DryCleaningScreens/EditDryCleaning';

import { CreateDryCleaning } from '../Screens/AdminScreens/DryCleaningScreens/CreateDryCleaning';

import { OrderList } from '../Screens/AdminScreens/OrderScreens/OrderList';
import { Order } from '../Screens/AdminScreens/OrderScreens/Order';
import { EditOrder } from '../Screens/AdminScreens/OrderScreens/EditOrder';

import { Client } from '../Screens/AdminScreens/ClientScreens/Client';
import { ClientList } from '../Screens/AdminScreens/ClientScreens/ClientsList';

import { HeaderRightCustom } from '../Components/HeaderRightCustom';

const DryCleaningStack = createStackNavigator();
const DryCleaningCreateStack = createStackNavigator();
const OrderNavigatorStack = createStackNavigator();
const ClientsNavigatorStack = createStackNavigator();
const AdminDrawer = createDrawerNavigator();

const DryCleaningNavigation = () => (
    <DryCleaningStack.Navigator>
        <DryCleaningStack.Screen
            name="DryCleaningList"
            component={DryCleaningList}
            options={{
                headerRight: (props) => <HeaderRightCustom {...props} />,
            }}
        />
        <DryCleaningStack.Screen
            name="DryCleaning"
            component={DryCleaning}
            options={({ route }) => ({ title: route.params.name })}
        />
        <DryCleaningStack.Screen
            name="EditDryCleaning"
            component={EditDryCleaning}
            options={({ route }) => ({ title: route.params.name })}
        />
    </DryCleaningStack.Navigator>
);

const DryCleaningCreateNavigation = () => (
    <DryCleaningCreateStack.Navigator>
        <DryCleaningCreateStack.Screen
            name="CreateDryCleaning"
            component={CreateDryCleaning}
            options={{
                headerRight: (props) => <HeaderRightCustom {...props} />,
            }}
        />
    </DryCleaningCreateStack.Navigator>
);

const OrderNavigator = () => (
    <OrderNavigatorStack.Navigator>
        <OrderNavigatorStack.Screen
            name="Orders"
            component={OrderList}
            options={{
                headerRight: (props) => <HeaderRightCustom {...props} />,
            }}
        />
        <OrderNavigatorStack.Screen
            name="Order"
            component={Order}
            options={{
                headerRight: (props) => <HeaderRightCustom {...props} />,
            }}
        />
        <OrderNavigatorStack.Screen
            name="EditOrder"
            component={EditOrder}
            options={{
                headerRight: (props) => <HeaderRightCustom {...props} />,
            }}
        />
    </OrderNavigatorStack.Navigator>
);

const ClientsNavigator = () => (
    <ClientsNavigatorStack.Navigator>
        <ClientsNavigatorStack.Screen
            name="Clients"
            component={ClientList}
            options={{
                headerRight: (props) => <HeaderRightCustom {...props} />,
            }}
        />
        <ClientsNavigatorStack.Screen
            name="Client"
            component={Client}
            options={({ route }) => ({ title: route.params.name })}
        />
    </ClientsNavigatorStack.Navigator>
);

/* const ServiceTypeNavigator = () => (
    <ServiceTypeNavigatorStack.Navigator>
        <ServiceTypeNavigatorStack.Screen
            name="ServiceTypes"
            component={ServiceTypes}
        />
        <ServiceTypeNavigatorStack.Screen
            name="ServiceType"
            component={ServiceType}
        />
        <ServiceTypeNavigatorStack.Screen
            name="EditServiceType"
            component={EditServiceType}
        />
    </ServiceTypeNavigatorStack.Navigator>
); */

export const AdminNavigator = () => (
    <NavigationContainer>
        <AdminDrawer.Navigator>
            <AdminDrawer.Screen
                name="DryCleaning"
                component={DryCleaningNavigation}
            />
            <AdminDrawer.Screen name="Client" component={ClientsNavigator} />
            <AdminDrawer.Screen name="Order" component={OrderNavigator} />
            <AdminDrawer.Screen
                name="DryCleaningCreate"
                component={DryCleaningCreateNavigation}
            />
        </AdminDrawer.Navigator>
    </NavigationContainer>
);
