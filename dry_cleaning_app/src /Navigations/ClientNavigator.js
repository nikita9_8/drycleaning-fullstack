import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';

import { DryCleaningList } from '../Screens/AdminScreens/DryCleaningScreens/DryCleaningList';
import { DryCleaning } from '../Screens/AdminScreens/DryCleaningScreens/DryCleaning';

import { OrderList } from '../Screens/AdminScreens/OrderScreens/OrderList';
import { Order } from '../Screens/AdminScreens/OrderScreens/Order';
import { EditOrder } from '../Screens/AdminScreens/OrderScreens/EditOrder';

import { ClientHeaderRightCustom } from '../Components/ClientHeaderRightCustom';

const DryCleaningStack = createStackNavigator();
const OrderNavigatorStack = createStackNavigator();
const AdminDrawer = createDrawerNavigator();

const DryCleaningNavigation = () => (
    <DryCleaningStack.Navigator>
        <DryCleaningStack.Screen
            name="DryCleaningList"
            component={DryCleaningList}
            options={{
                headerRight: (props) => <ClientHeaderRightCustom {...props} />,
            }}
        />
        <DryCleaningStack.Screen
            name="DryCleaning"
            component={DryCleaning}
            options={{
                // headerTitle: ({ route }) => ({ title: route.params.name }),
                headerRight: (props) => <ClientHeaderRightCustom {...props} />,
            }}
        />
    </DryCleaningStack.Navigator>
);

const OrderNavigator = () => (
    <OrderNavigatorStack.Navigator>
        <OrderNavigatorStack.Screen
            name="Orders"
            component={OrderList}
            options={{
                headerRight: (props) => <ClientHeaderRightCustom {...props} />,
            }}
        />
        <OrderNavigatorStack.Screen
            name="Order"
            component={Order}
            options={{
                headerRight: (props) => <ClientHeaderRightCustom {...props} />,
            }}
        />
        <OrderNavigatorStack.Screen
            name="EditOrder"
            component={EditOrder}
            options={{
                headerRight: (props) => <ClientHeaderRightCustom {...props} />,
            }}
        />
    </OrderNavigatorStack.Navigator>
);

/* const OrderCreateNavigator = () => (
    <OrderCreateNavigatorStack.Navigator>
        <OrderCreateNavigatorStack.Screen
            name="CreateOrder"
            component={CreateOrder}
            options={{
                headerRight: (props) => <ClientHeaderRightCustom {...props} />,
            }}
        />
    </OrderCreateNavigatorStack.Navigator>
); */

export const ClientNavigator = () => (
    <NavigationContainer>
        <AdminDrawer.Navigator>
            <AdminDrawer.Screen
                name="DryCleaning"
                component={DryCleaningNavigation}
            />
            <AdminDrawer.Screen name="Order" component={OrderNavigator} />
        </AdminDrawer.Navigator>
    </NavigationContainer>
);
