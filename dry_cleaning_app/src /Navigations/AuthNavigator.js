import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { Login } from '../Screens/AuthScreens/Login';
import { Register } from '../Screens/AuthScreens/Register';
import { Restore } from '../Screens/AuthScreens/Restore';

const Stack = createStackNavigator();

export const AuthNavigation = () => (
    <NavigationContainer>
        <Stack.Navigator>
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="Register" component={Register} />
            <Stack.Screen name="Restore" component={Restore} />
        </Stack.Navigator>
    </NavigationContainer>
);
