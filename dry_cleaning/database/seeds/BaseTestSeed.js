'use strict';

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory');
const DryCleaningService = use('App/Models/DryCleaningService');

class BaseTestSeed {
    async run() {
        const user = await Factory.model('App/Models/User').create({
            role: 'client',
        });
        const dryCleaning = await Factory.model(
            'App/Models/DryCleaning'
        ).create();
        const photos = await Factory.model('App/Models/Photo').makeMany(2);
        const service = await Factory.model('App/Models/Service').make();
        const serviceType = await Factory.model(
            'App/Models/ServiceType'
        ).create();

        await dryCleaning.photos().saveMany(photos);

        await dryCleaning.services().save(service, (row) => {
            row.cost = 200;
            row.service_type_id = serviceType.id;
        });

        const order = await Factory.model('App/Models/Order').create({
            dry_cleaning_id: dryCleaning.id,
            user_id: user.id,
            status: 'in progress',
        });

        const dryCleaningService = await DryCleaningService.findBy(
            'dry_cleaning_id',
            dryCleaning.id
        );

        await order.dryCleaningServices().attach([dryCleaningService.id]);
    }
}

module.exports = BaseTestSeed;
