'use strict';

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory');

Factory.blueprint('App/Models/User', (faker, i, data) => {
    return {
        username: faker.username(),
        email: faker.email(),
        password: faker.password(),
        role: data.role,
        money: faker.integer({ min: 20, max: 2000 }),
    };
});

Factory.blueprint('App/Models/DryCleaning', (faker, i, data) => {
    return {
        name: faker.word(),
        description: faker.paragraph(),
    };
});

Factory.blueprint('App/Models/Photo', (faker) => {
    return {
        photo: faker.url({ path: 'image' }),
    };
});

Factory.blueprint('App/Models/Service', (faker) => {
    return {
        name: faker.word(),
    };
});

Factory.blueprint('App/Models/ServiceType', (faker) => {
    return {
        name: faker.word(),
    };
});

Factory.blueprint('App/Models/Order', (faker, i, data) => {
    return {
        dry_cleaning_id: data.dry_cleaning_id,
        user_id: data.user_id,
        cost: faker.integer({ min: 20, max: 2000 }),
        status: data.status,
    };
});
