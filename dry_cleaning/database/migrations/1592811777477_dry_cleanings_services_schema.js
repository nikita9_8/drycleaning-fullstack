'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class DryCleaningsServicesSchema extends Schema {
    up() {
        this.create('dry_cleaning_services', (table) => {
            table.increments();
            table
                .integer('service_type_id')
                .unsigned()
                .references('id')
                .inTable('service_types')
                .onDelete('cascade');
            table
                .integer('service_id')
                .unsigned()
                .references('id')
                .inTable('services')
                .onDelete('cascade');
            table
                .integer('dry_cleaning_id')
                .unsigned()
                .references('id')
                .inTable('dry_cleanings')
                .onDelete('cascade');
            table.integer('cost').notNullable();
            table.timestamps();
        });
    }

    down() {
        this.drop('dry_cleaning_services');
    }
}

module.exports = DryCleaningsServicesSchema;
