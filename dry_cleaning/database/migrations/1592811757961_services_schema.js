'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class ServicesSchema extends Schema {
    up() {
        this.create('services', (table) => {
            table.increments();
            table.string('name', 40).notNullable().unique();
            table.timestamps();
        });
    }

    down() {
        this.drop('services');
    }
}

module.exports = ServicesSchema;
