'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class OrderDryCleaningServicesSchema extends Schema {
    up() {
        this.create('order_dry_cleaning_services', (table) => {
            table.increments();
            table
                .integer('order_id')
                .unsigned()
                .references('id')
                .inTable('orders')
                .onDelete('cascade');
            table
                .integer('dry_cleaning_services_id')
                .unsigned()
                .references('id')
                .inTable('dry_cleaning_services')
                .onDelete('cascade');
            table.integer('cost').notNullable();
            table.timestamps();
        });
    }

    down() {
        this.drop('order_dry_cleaning_services');
    }
}

module.exports = OrderDryCleaningServicesSchema;
