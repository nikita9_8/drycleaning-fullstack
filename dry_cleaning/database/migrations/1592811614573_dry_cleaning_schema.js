'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DryCleaningSchema extends Schema {
  up () {
    this.create('dry_cleanings', (table) => {
      table.increments();
      table.string('name', 30).notNullable().unique();
      table.text('description');
      table.timestamps();
    })
  }

  down () {
    this.drop('dry_cleanings')
  }
}

module.exports = DryCleaningSchema
