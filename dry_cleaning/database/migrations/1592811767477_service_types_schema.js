'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class ServiceTypesSchema extends Schema {
    up() {
        this.create('service_types', (table) => {
            table.increments();
            table.string('name', 40).notNullable().unique();
            table.timestamps();
        });
    }

    down() {
        this.drop('service_types');
    }
}

module.exports = ServiceTypesSchema;
