'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class OrdersSchema extends Schema {
    up() {
        this.create('orders', (table) => {
            table.increments();
            table
                .integer('dry_cleaning_id')
                .unsigned()
                .references('id')
                .inTable('dry_cleanings')
                .onDelete('cascade');
            table
                .integer('user_id')
                .unsigned()
                .references('id')
                .inTable('users')
                .onDelete('cascade');
            table.string('user_info', 80).notNullable();
            table.integer('cost').notNullable();
            table.string('status', 50).notNullable();
            table.text('comment').nullable();
            table.timestamps();
        });
    }

    down() {
        this.drop('orders');
    }
}

module.exports = OrdersSchema;
