'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PhotosSchema extends Schema {
  up () {
    this.create('photos', (table) => {
      table.increments();
      table.integer('dry_cleaning_id').unsigned().references('id').inTable('dry_cleanings').onDelete('cascade');
      table.text('photo').notNullable();
      table.timestamps();
    })
  }

  down () {
    this.drop('photos')
  }
}

module.exports = PhotosSchema
