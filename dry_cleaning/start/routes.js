/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route');

Route.get('/', () => {
    return { greeting: 'Hello world in JSON' };
});

// For guests
Route.group(() => {
    Route.post('register', 'UserController.register');
    Route.post('login', 'UserController.login');
    Route.post('restore', 'UserController.restorePassword');
}).prefix('api');

// For Admin/Client
Route.group(() => {
    Route.get('logout', 'UserController.logout');

    Route.get('users/:id', 'UserController.show');

    // DryCleaning routes
    Route.get('drycleanings', 'DryCleaningController.index');
    Route.get('drycleanings/:id', 'DryCleaningController.show');
    Route.post(
        'drycleanings/create',
        'DryCleaningController.create'
    ).middleware(['admin']);
    Route.put('drycleanings/:id', 'DryCleaningController.update').middleware([
        'admin',
    ]);
    Route.delete(
        'drycleanings/:id',
        'DryCleaningController.delete'
    ).middleware(['admin']);

    // Order routes
    Route.get('orders', 'OrderController.index');
    Route.get('orders/:id', 'OrderController.show');
    Route.post('orders/create', 'OrderController.create');
    Route.put('orders/:id', 'OrderController.update');
    Route.delete('orders/:id', 'OrderController.delete');
})
    .prefix('api')
    .middleware(['auth']);

// For Admin
Route.group(() => {
    // List of clients for administrator
    Route.get('clients', 'UserController.clients');

    // ServiceType routes
    Route.get('serticetypes/', 'ServiceTypeController.index');
    Route.get('serticetypes/:id', 'ServiceTypeController.show');
    Route.post('serticetypes/create', 'ServiceTypeController.create');
    Route.put('serticetypes/:id', 'ServiceTypeController.update');
    Route.delete('serticetypes/:id', 'ServiceTypeController.delete');
})
    .prefix('api')
    .middleware(['auth', 'admin']);
