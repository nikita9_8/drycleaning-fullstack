const Database = use('Database');
const DryCleaning = use('App/Models/DryCleaning');
const ServiceType = use('App/Models/ServiceType');
const Service = use('App/Models/Service');
const DryCleaningService = use('App/Models/DryCleaningService');
const Photo = use('App/Models/Photo');

class DryCleaningController {
    async index({ response }) {
        const dryCleanings = await DryCleaning.query()
            .with('dryCleaningServices', (builder) => {
                builder.with('service');
                builder.with('serviceType');
            })
            .with('photos')
            .fetch();

        response.status(200).send(dryCleanings);
    }

    async create({ request, response }) {
        const data = request.only([
            'name',
            'description',
            'serviceTypes',
            'photos',
        ]);

        try {
            const dryCleaning = await DryCleaning.create({
                name: data.name,
                description: data.description,
            });

            await dryCleaning.photos().createMany(JSON.parse(data.photos));

            const serviceTypesData = JSON.parse(data.serviceTypes);
            const serviceTypesGroupInstances = [];

            for (const { serviceType, services } of serviceTypesData) {
                const serviceTypesGroupInstance = {
                    serviceType: await ServiceType.findOrCreate(
                        { name: serviceType },
                        { name: serviceType }
                    ),
                };
                const servicesInstances = await Promise.all(
                    services.map(({ name }) =>
                        Service.findOrCreate({ name }, { name })
                    )
                );
                serviceTypesGroupInstance.services = servicesInstances.map(
                    (service, index) => ({
                        id: service.id,
                        cost: services[index].cost,
                    })
                );

                serviceTypesGroupInstances.push(serviceTypesGroupInstance);
            }

            await Promise.all(
                serviceTypesGroupInstances
                    .map(({ serviceType, services }) =>
                        services.map((service) =>
                            DryCleaningService.create({
                                service_type_id: serviceType.id,
                                service_id: service.id,
                                dry_cleaning_id: dryCleaning.id,
                                cost: service.cost,
                            })
                        )
                    )
                    .flat()
            );

            const responseData = await DryCleaning.query()
                .where('id', dryCleaning.id)
                .with('dryCleaningServices', (builder) => {
                    builder.with('service');
                    builder.with('serviceType');
                })
                .with('photos')
                .fetch();

            response.status(201).send(responseData);
        } catch (e) {
            response.status(400).send(e.message);
        }
    }

    async update({ params, request, response }) {
        const { id } = params;
        const data = request.only([
            'name',
            'description',
            'serviceTypes',
            'photos',
        ]);

        try {
            // Update DryCleaning
            const dryCleaning = await DryCleaning.findOrFail(id);

            dryCleaning.merge({
                name: data.name,
                description: data.description,
            });

            await dryCleaning.save();

            // Change Photos
            if (data.photos) {
                const parcedPhotos = JSON.parse(data.photos);

                if (parcedPhotos.length) {
                    await Photo.query()
                        .where('dry_cleaning_id', dryCleaning.id)
                        .delete();

                    await dryCleaning.photos().createMany(parcedPhotos);
                }
            }

            // Update Service and ServiceType
            const serviceTypesData = JSON.parse(data.serviceTypes);
            const serviceTypesGroupInstances = [];

            for (const { serviceType, services } of serviceTypesData) {
                const serviceTypesGroupInstance = {
                    serviceType: await ServiceType.findOrCreate(
                        { name: serviceType },
                        { name: serviceType }
                    ),
                };
                const servicesInstances = await Promise.all(
                    services.map(({ name }) =>
                        Service.findOrCreate({ name }, { name })
                    )
                );
                serviceTypesGroupInstance.services = servicesInstances.map(
                    (service, index) => ({
                        id: service.id,
                        cost: services[index].cost,
                    })
                );

                serviceTypesGroupInstances.push(serviceTypesGroupInstance);
            }

            // Update relation for DryCleaningService
            const dryCleaningServicesInstances = serviceTypesGroupInstances.map(
                ({ serviceType, services }) => ({
                    serviceType,
                    services,
                    dryCleaningServices: services.map((service) =>
                        DryCleaningService.findOrCreate(
                            {
                                service_type_id: serviceType.id,
                                service_id: service.id,
                                dry_cleaning_id: dryCleaning.id,
                            },
                            {
                                service_type_id: serviceType.id,
                                service_id: service.id,
                                dry_cleaning_id: dryCleaning.id,
                                cost: service.cost,
                            }
                        )
                    ),
                })
            );

            const updatedDryCleaningServicesInstancesIds = [];

            for (const {
                serviceType,
                services,
                dryCleaningServices,
            } of dryCleaningServicesInstances) {
                const dryCleaningServicesInstances = await Promise.all(
                    dryCleaningServices
                );

                const updateDryCleaningServicesInstances = [];

                dryCleaningServicesInstances.forEach((dryCleaningService) => {
                    const service = services.find(
                        (item) => item.id === dryCleaningService.service_id
                    );

                    if (
                        service &&
                        serviceType.id === dryCleaningService.service_type_id
                    ) {
                        dryCleaningService.merge({
                            cost: service.cost,
                        });

                        updatedDryCleaningServicesInstancesIds.push(
                            dryCleaningService.id
                        );

                        updateDryCleaningServicesInstances.push(
                            dryCleaningService.save()
                        );
                    }
                });
                await Promise.all(updateDryCleaningServicesInstances);
            }

            await Database.from('dry_cleaning_services')
                .where('dry_cleaning_id', dryCleaning.id)
                .whereNotIn('id', updatedDryCleaningServicesInstancesIds)
                .delete();

            await this.show({ params, request, response });
        } catch (e) {
            response.status(400).send(e.message);
        }
    }

    async show({ params, response }) {
        const { id } = params;
        const dryCleaning = await DryCleaning.query()
            .where('id', id)
            .with('dryCleaningServices', (builder) => {
                builder.with('service');
                builder.with('serviceType');
            })
            .with('photos')
            .fetch();

        response.status(200).send(dryCleaning);
    }

    async delete({ params, response }) {
        const { id } = params;
        const dryCleaning = await DryCleaning.find(id);

        await dryCleaning.delete();

        response.status(200).send({ status: 'OK' });
    }
}

module.exports = DryCleaningController;
