'use strict';
const ServiceType = use('App/Models/ServiceType');

class ServiceTypeController {
    async index({ response }) {
        const serviceTypes = await ServiceType.all();

        response.status(200).send(serviceTypes);
    }

    async show({ params, response }) {
        const { id } = params;
        const serviceType = await ServiceType.find(id);

        response.status(200).send(serviceType);
    }

    async create({ request, response }) {
        const data = request.only(['name']);
        try {
            const serviceType = await ServiceType.findByOrFail(
                'name',
                data.name
            );

            response.status(201).send(serviceType);
        } catch (e) {
            response.status(400).send(e.message);
        }
    }

    async update({ params, request, response }) {
        const { id } = params;
        const data = request.only(['name']);

        try {
            const serviceType = await ServiceType.findOrFail(id);
            serviceType.merge({
                name: data.name,
            });

            await serviceType.save();

            const updatedServiceType = await ServiceType.findOrFail(id);

            response.status(200).send(updatedServiceType);
        } catch (e) {
            response.status(400).send(e.message);
        }
    }

    async delete({ params, response }) {
        const { id } = params;
        const serviceType = await ServiceType.find(id);

        await serviceType.delete();

        response.status(200).send({ status: 'OK' });
    }
}

module.exports = ServiceTypeController;
