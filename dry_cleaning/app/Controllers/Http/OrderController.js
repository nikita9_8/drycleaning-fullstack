const Database = use('Database');
const Order = use('App/Models/Order');
const User = use('App/Models/User');
const Service = use('App/Models/Service');
const ServiceType = use('App/Models/ServiceType');
const DryCleaningService = use('App/Models/DryCleaningService');
const DryCleaning = use('App/Models/DryCleaning');

class OrderController {
    async index({ response, auth }) {
        const user = await auth.getUser();
        let orders = Order.query()
            .with('dryCleaningServices', (builder) => {
                builder.with('service');
                builder.with('serviceType');
            })
            .with('dryCleaning')
            .with('user');

        if (user.role === 'admin') {
            orders = orders.fetch();
        } else {
            orders = orders.where('user_id', user.id).fetch();
        }

        orders = await orders;

        response.status(200).send(orders);
    }

    async show({ params, response, auth }) {
        const { id } = params;
        const user = await auth.getUser();

        let order = await Order.query()
            .where('id', id)
            .with('dryCleaningServices', (builder) => {
                builder.with('service');
                builder.with('serviceType');
            })
            .with('dryCleaning')
            .with('user')
            .fetch();
        order = await order.first();

        if (user.role === 'admin' && order.status === 'in precessing') {
            order.status = 'new';
        } else if (
            user.role === 'client' &&
            order.status === 'ready for extradition'
        ) {
            order.status = 'completed';
        }

        response.status(200).send(order);
    }

    async create({ request, response, auth }) {
        const data = request.only([
            'user_id',
            'dry_cleaning_id',
            'status',
            'serviceGroup',
        ]);

        try {
            // User from auth
            const user = await auth.getUser();
            // DryCleaningService relation
            const serviceGroupData = JSON.parse(data.serviceGroup);

            const serviceGroupDataInstances = [];

            for (const { serviceType, services } of serviceGroupData) {
                const serviceGroupDataInstance = {
                    serviceType: await ServiceType.findByOrFail(
                        'name',
                        serviceType
                    ),
                };
                const servicesInstances = await Promise.all(
                    services.map(({ name }) =>
                        Service.findByOrFail('name', name)
                    )
                );
                serviceGroupDataInstance.services = servicesInstances.map(
                    (service, index) => ({
                        id: service.id,
                        cost: services[index].cost,
                    })
                );

                serviceGroupDataInstances.push(serviceGroupDataInstance);
            }
            let dryCleaningServicesInstances = serviceGroupDataInstances
                .map(({ serviceType, services }) =>
                    services.map(({ id: serviceId }) =>
                        DryCleaningService.query()
                            .where('dry_cleaning_id', data.dry_cleaning_id)
                            .where('service_type_id', serviceType.id)
                            .where('service_id', serviceId)
                            .fetch()
                    )
                )
                .flat();

            dryCleaningServicesInstances = await Promise.all(
                dryCleaningServicesInstances
            );
            let orderCost = 0;

            const orderDryCleaningServicesData = dryCleaningServicesInstances.map(
                (dryCleaningServicesInstance) => {
                    const instance = dryCleaningServicesInstance.first();
                    orderCost += +instance.cost;

                    return {
                        id: instance.id,
                        cost: instance.cost,
                    };
                }
            );

            // DryCleaning
            const dryCleaning = await DryCleaning.findOrFail(
                data.dry_cleaning_id
            );

            // Order create
            const order = await Order.create({
                dry_cleaning_id: dryCleaning.id,
                user_id: user.id,
                user_info: user.username,
                cost: orderCost,
                status: 'in precessing',
            });

            user.merge({
                money: user.money - orderCost,
            });

            await user.save();

            await order.dryCleaningServices().attach(
                orderDryCleaningServicesData.map((i) => i.id),
                (row) => {
                    const serviceWithCost = orderDryCleaningServicesData.find(
                        (item) => item.id === row.dry_cleaning_services_id
                    );

                    if (serviceWithCost) {
                        row.cost = serviceWithCost.cost;
                    }
                }
            );

            const orderData = await Order.query()
                .where('id', order.id)
                .with('user')
                .with('dryCleaning', (builder) => builder.with('photos'))
                .with('dryCleaningServices', (builder) =>
                    builder.with('service').with('serviceType')
                )
                .fetch();

            response.status(201).send(orderData);
        } catch (e) {
            response.status(400).send(e.message);
        }
    }

    async update({ params, request, response, auth }) {
        const { id } = params;
        const data = request.only([
            'user_info',
            'created_at',
            'servicesGroup',
            'status',
            'comment',
        ]);

        try {
            const requestedUser = await auth.getUser();

            if (
                requestedUser.role === 'client' &&
                data.status === 'completed'
            ) {
                const order = await Order.findOrFail(id);
                order.merge({
                    status: data.status,
                });

                await order.save();

                await this.show({ params, response, auth });
            }

            if (requestedUser.role === 'admin') {
                const order = await Order.findOrFail(id);

                // DryCleaningService relation
                const serviceGroupData = JSON.parse(data.servicesGroup);

                const serviceGroupDataInstances = [];

                for (const { serviceType, services } of serviceGroupData) {
                    const serviceGroupDataInstance = {
                        serviceType: await ServiceType.findByOrFail(
                            'name',
                            serviceType
                        ),
                    };
                    const servicesInstances = await Promise.all(
                        services.map(({ name }) =>
                            Service.findByOrFail('name', name)
                        )
                    );
                    serviceGroupDataInstance.services = servicesInstances.map(
                        (service, index) => ({
                            id: service.id,
                            cost: services[index].cost,
                        })
                    );

                    serviceGroupDataInstances.push(serviceGroupDataInstance);
                }
                let dryCleaningServicesInstances = serviceGroupDataInstances
                    .map(({ serviceType, services }) =>
                        services.map(({ id: serviceId }) =>
                            DryCleaningService.query()
                                .where('dry_cleaning_id', order.dry_cleaning_id)
                                .where('service_type_id', serviceType.id)
                                .where('service_id', serviceId)
                                .fetch()
                        )
                    )
                    .flat();

                dryCleaningServicesInstances = await Promise.all(
                    dryCleaningServicesInstances
                );
                let orderCost = 0;

                const ordersDryServicePivot = serviceGroupDataInstances
                    .map(({ services }) => services)
                    .flat();

                const orderDryCleaningServicesData = dryCleaningServicesInstances.map(
                    (item) => {
                        const instance = item.first();
                        const orderDryServicePivot = ordersDryServicePivot.find(
                            (orderPivot) =>
                                orderPivot.id === instance.service_id
                        );
                        const newOrderPivotData = {
                            id: instance.id,
                        };

                        if (orderDryServicePivot) {
                            newOrderPivotData.cost = orderDryServicePivot.cost;
                        } else {
                            newOrderPivotData.cost = instance.cost;
                        }
                        orderCost += +newOrderPivotData.cost;

                        return newOrderPivotData;
                    }
                );
                const user = await User.findOrFail(order.user_id);
                let newUserMoney = user.money + order.cost - orderCost;

                if (newUserMoney < 0) {
                    throw new Error('Not enough money');
                }

                if (data.status === 'return' && !data.comment) {
                    throw new Error('Write reason for returning order');
                }

                if (data.status === 'return') {
                    newUserMoney = user.money + order.cost;
                }

                await Database.from('order_dry_cleaning_services')
                    .where('order_id', order.id)
                    .delete();

                // Order update
                order.merge({
                    user_info: data.user_info || user.username,
                    cost: orderCost,
                    status: data.status,
                    comment: data.comment || null,
                    created_at: data.created_at || order.created_at,
                });

                await order.save();

                user.merge({
                    money: newUserMoney,
                });

                await user.save();

                // Create new relations
                await order.dryCleaningServices().attach(
                    orderDryCleaningServicesData.map((i) => i.id),
                    (row) => {
                        const serviceWithCost = orderDryCleaningServicesData.find(
                            (item) => item.id === row.dry_cleaning_services_id
                        );

                        if (serviceWithCost) {
                            row.cost = serviceWithCost.cost;
                        }
                    }
                );

                await this.show({ params, response, auth });
            }
        } catch (e) {
            response.status(400).send(e.message);
        }
    }

    async delete({ params, response }) {
        const { id } = params;

        try {
            const order = await Order.findOrFail(id);
            const user = await User.findOrFail(order.user_id);
            const orderMoney = order.cost;

            await order.delete();

            if (['return', 'completed'].indexOf(order.status) === -1) {
                const userMoney = user.money + orderMoney;

                user.merge({
                    money: userMoney,
                });
                await user.save();
            }

            response.status(200).send({ status: 'OK' });
        } catch (e) {
            response.status(200).send(e.message);
        }
    }
}

module.exports = OrderController;
