const User = use('App/Models/User');

class UserController {
    async clients({ response }) {
        const clientUsers = await User.query().where('role', 'client').fetch();

        response.status(200).send(clientUsers);
    }

    async register({ request, response, auth }) {
        const userData = request.only([
            'email',
            'password',
            'username',
            'role',
            'money',
        ]);

        try {
            const user = await User.create(userData);

            user.token = await auth.generate(user);

            response.status(201).send(user);
        } catch (e) {
            response.status(400).send(e.message);
        }
    }

    async login({ request, response, auth }) {
        try {
            const { email, password } = request.only(['email', 'password']);
            const token = await auth.attempt(email, password);
            const user = await User.findByOrFail('email', email);

            user.token = token;

            response.status(202).send(user);
        } catch (e) {
            response.status(400).send(e.message);
        }
    }

    async show({ response, auth }) {
        try {
            const user = await auth.getUser();

            response.status(200).send(user);
        } catch (e) {
            response.status(401).send(e.message);
        }
    }

    async logout({ response, auth }) {
        try {
            const user = await auth.getUser();
            await auth.revokeTokensForUser(user);

            response.status(200).send(true);
        } catch (e) {
            response.status(401).send('Missing or invalid token');
        }
    }

    async restorePassword({ request, response, auth }) {
        try {
            const { email, password } = request.only(['email', 'password']);
            const user = await User.findByOrFail('email', email);
            user.password = password;

            await user.save();

            const token = await auth.generate(user);
            user.token = token;

            response.status(200).send(user);
        } catch (e) {
            response.status(400).send(e.message);
        }
    }
}

module.exports = UserController;
