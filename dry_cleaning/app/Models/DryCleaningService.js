'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class DryCleaningService extends Model {
    static get table() {
        return 'dry_cleaning_services';
    }

    dryCleaning() {
        return this.belongsTo(
            'App/Models/DryCleaning',
            'dry_cleaning_id',
            'id'
        );
    }

    service() {
        return this.belongsTo('App/Models/Service', 'service_id', 'id');
    }

    serviceType() {
        return this.belongsTo(
            'App/Models/ServiceType',
            'service_type_id',
            'id'
        );
    }

    orders() {
        return this.belongsToMany(
            'App/Models/Order',
            'dry_cleaning_services_id',
            'order_id',
            'id',
            'id'
        ).pivotModel('App/Models/OrderDryCleaningService');
    }
}

module.exports = DryCleaningService;
