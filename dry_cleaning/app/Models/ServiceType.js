'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class ServiceType extends Model {
    static get table() {
        return 'service_types';
    }

    dryCleanings() {
        return this.belongsToMany(
            'App/Models/DryCleaning',
            'service_type_id',
            'dry_cleaning_id',
            'id',
            'id'
        ).pivotModel('App/Models/DryCleaningService');
    }
}

module.exports = ServiceType;
