'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Order extends Model {
    static get table() {
        return 'orders';
    }

    user() {
        return this.belongsTo('App/Models/User', 'user_id', 'id');
    }

    dryCleaning() {
        return this.belongsTo(
            'App/Models/DryCleaning',
            'dry_cleaning_id',
            'id'
        );
    }

    dryCleaningServices() {
        return this.belongsToMany(
            'App/Models/DryCleaningService',
            'order_id',
            'dry_cleaning_services_id',
            'id',
            'id'
        )
            .withPivot(['cost'])
            .pivotModel('App/Models/OrderDryCleaningService');
    }

    orderDryCleaningService() {
        return this.hasMany(
            'App/Models/OrderDryCleaningService',
            'id',
            'order_id'
        );
    }
}

module.exports = Order;
