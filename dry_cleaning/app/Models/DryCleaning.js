'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class DryCleaning extends Model {
    static get table() {
        return 'dry_cleanings';
    }

    photos() {
        return this.hasMany('App/Models/Photo', 'id', 'dry_cleaning_id');
    }

    orders() {
        return this.hasMany('App/Models/Order', 'id', 'dry_cleaning_id');
    }

    dryCleaningServices() {
        return this.hasMany(
            'App/Models/DryCleaningService',
            'id',
            'dry_cleaning_id'
        );
    }

    services() {
        return this.belongsToMany(
            'App/Models/Service',
            'dry_cleaning_id',
            'service_id',
            'id',
            'id'
        )
            .pivotModel('App/Models/DryCleaningService')
            .withPivot(['cost']);
    }

    serviceTypes() {
        return this.belongsToMany(
            'App/Models/ServiceType',
            'dry_cleaning_id',
            'service_type_id',
            'id',
            'id'
        )
            .pivotModel('App/Models/DryCleaningService')
            .withPivot(['cost']);
    }
}

module.exports = DryCleaning;
