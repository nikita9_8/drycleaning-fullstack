'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Service extends Model {
    static get table() {
        return 'services';
    }

    dryCleanings() {
        return this.belongsToMany(
            'App/Models/DryCleaning',
            'service_id',
            'dry_cleaning_id',
            'id',
            'id'
        ).pivotModel('App/Models/DryCleaningService');
    }
}

module.exports = Service;
