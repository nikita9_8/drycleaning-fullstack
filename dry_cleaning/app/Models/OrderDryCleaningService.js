'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class OrderDryCleaningService extends Model {
    static get table() {
        return 'order_dry_cleaning_services';
    }

    order() {
        return this.belongsTo('App/Models/Order', 'order_id', 'id');
    }

    dryCleaningService() {
        return this.belongsTo(
            'App/Models/DryCleaningService',
            'dry_cleaning_services_id',
            'id'
        );
    }
}

module.exports = OrderDryCleaningService;
