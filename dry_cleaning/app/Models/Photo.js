'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Photo extends Model {
  static get table () {
    return 'photos';
  }

  dryCleaning () {
    return this.belongsTo('App/Models/DryCleaning', 'dry_cleaning_id', 'id');
  }
}

module.exports = Photo
