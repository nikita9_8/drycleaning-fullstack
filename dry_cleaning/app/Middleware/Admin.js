'use strict';
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class Admin {
    /**
     * @param {object} ctx
     * @param {Function} next
     */
    async handle({ request, auth, response }, next) {
        const user = await auth.getUser();

        if (user.role !== 'admin') {
            return response.status(401).send("You don't have access");
        }
        request.user = user;
        await next();
    }
}

module.exports = Admin;
